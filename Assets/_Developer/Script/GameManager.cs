using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public static Action<Texture2D> OnProfPicSuccessfullyFetched;
    public static Action OnUpdateCoin, OnUpdatePowerup;
    public static Action OnQdavantureQuizReceived;

    public CalanderQuizdata calanderQuizdata;

    public List<powerUpDetails> powerUpPurchaseData;

    public CurrentGame currentGameQuiz;

    public QuestionDetail quizOfTheDay;

    //   public List<calanderAPIdata> calanderAPIdatas;

    public List<QuizDetail> allQuizes;
    public int currentSelectedQuizNumber;

    [SerializeField]
    private QuizAdvanture_QuizSelection quizAdvanture_QuizSelection;

    [SerializeField]
    private QuizListScreen quizListScreen;

    public QuizAdvantureScreen advantureScreen;

    public QuesOfDayScreen quesOfDay;

    [SerializeField]
    private PlayerProfileScreen playerProfileScreen;

    [SerializeField]
    public ShopPanel shopPanel;

    public HomeScreen homeScreen;



    private void Awake()
    {
        instance = this;
        NetworkManager.instance.playerDetails = new PlayerDetails();
        allQuizes = new List<QuizDetail>();
        //   calanderAPIdatas = new List<calanderAPIdata>();
        calanderQuizdata = new CalanderQuizdata();

    }
    public void OnEnable()
    {

        quizAdvanture_QuizSelection.subscribeEvent();
        advantureScreen.subscribeEvent();
        quesOfDay.subscribeEvent();
        playerProfileScreen.subscribeEvent();
        shopPanel.subscribeEvent();
        homeScreen.subscribeEvent();
        quizListScreen.subscribeEvent();
        OnUpdatePowerup += PowerUpManager.instance.setPowerUp;          //-----


        OnUpdateCoin?.Invoke();
        OnUpdatePowerup?.Invoke();
    }

    public void OnDisable()
    {
        quizAdvanture_QuizSelection.unSubscribeEvent();
        quesOfDay.unSubscribeEvent();
        advantureScreen.unSubscribeEvent();
        playerProfileScreen.unSubscribeEvent();
        shopPanel.unSubscribeEvent();
        homeScreen.unSubscribeEvent();
        quizListScreen.unSubscribeEvent();

        OnUpdatePowerup -= PowerUpManager.instance.setPowerUp;          //-----
    }


    public void resetGameData()
    {
        calanderQuizdata = new CalanderQuizdata();
        currentGameQuiz = new CurrentGame();
        currentGameQuiz.quizAdvanture = new List<QuestionDetail>();
        currentGameQuiz.attentdedQuestionNumber = new List<int>();
        currentGameQuiz.totalCorrectAnswer = new List<int>();
        currentGameQuiz.totalInCorrectAnswer = new List<int>();
        currentGameQuiz.totalQuestion = 0;

        quizOfTheDay = null;
    }


    public void getAllQuiz()
    {


        //for (int i = 0; i < TotalNumOfQuiz; i++)
        //{
        //    UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        //    StartCoroutine(NetworkManager.instance.getQuizBy_ID(i.ToString(), i == TotalNumOfQuiz - 1));
        //}
    }
}

[Serializable]
public class CurrentGame
{
    public static CurrentGame instance;         //--------
    public List<QuestionDetail> quizAdvanture;
    public List<int> attentdedQuestionNumber = new();
    public List<int> totalCorrectAnswer = new();
    public List<int> totalInCorrectAnswer = new();
    public int totalQuestion;
}


[Serializable]
public class powerUpDetails
{
    public string powerUpName;
    public int powerUp_Qty;
    public int powerUp_Cost;
}

[Serializable]
public class CalanderQuizdata
{
    public List<calanderAPIdata> calanderAPIdatelist = new List<calanderAPIdata>();
    public DateTime startingDate, endingDate;

}


[Serializable]
public class calanderAPIdata
{
    public string _date;
    public DateTime date;
    public bool isCorrectAns;
}

