using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ResultScreen : MonoBehaviour
{
    public static ResultScreen instance;
    [SerializeField]
    private TextMeshProUGUI txtQuizNum, txtCorrectCntOutOfAll, txtTotQuestion, txtCorrectCnt;
    public TextMeshProUGUI txtNextQuizNum;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        //Debug.Log("Onenable results screen --> Value of playerpref -- unlockedQuiz = " + PlayerPrefs.GetInt("unlockedQuiz"));


        if (QuizAdvantureScreen.instance.scoreOfCurrentQuiz > QuizAdvantureScreen.instance.seventyFivePerScore && PlayerPrefs.GetInt("unlockedQuiz") == 0)            //----------
        {
            PlayerPrefs.SetInt("unlockedQuiz", 1);
            Debug.Log("-----------Next quiz unloked here----When player get score above 75%------Curr quiz Num : " + GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber].Quiz_Tite + "     unlocking quiz num is : " + GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber + 1].Quiz_Tite);

            StartCoroutine(NetworkManager.instance.unloackNextQuiz(0, GameManager.instance.currentSelectedQuizNumber));
        }
        else Debug.Log("Something else happened");

        txtQuizNum.text = "Quiz-" + GameManager.instance.currentSelectedQuizNumber;
        txtCorrectCntOutOfAll.text = GameManager.instance.currentGameQuiz.totalCorrectAnswer.Count + "/" + GameManager.instance.currentGameQuiz.totalQuestion;
        txtTotQuestion.text = "" + GameManager.instance.currentGameQuiz.totalQuestion;
        txtCorrectCnt.text = "" + GameManager.instance.currentGameQuiz.totalCorrectAnswer.Count;
        txtNextQuizNum.text = "Quiz-" + (GameManager.instance.currentSelectedQuizNumber + 1);

        //Debug.Log("Before : " + PlayerPrefs.GetInt("quiz") + "    Current quiz number  :  " + GameManager.instance.currentSelectedQuizNumber);

        //PlayerPrefs.SetInt("quiz" + GameManager.instance.currentSelectedQuizNumber + 1, 1);     //From quiz_ to quiz

        //Debug.Log("After : " + PlayerPrefs.GetInt("quiz") + "    Current quiz number  :  " + GameManager.instance.currentSelectedQuizNumber);

        if (PlayerPrefs.GetInt("unlockedQuiz") == 0)
        {
            for (int i = 4; i < 8; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
            //transform.GetChild(4).gameObject.SetActive(false);
            //transform.GetChild(5).gameObject.SetActive(false);
            //transform.GetChild(6).gameObject.SetActive(false);
            //transform.GetChild(7).gameObject.SetActive(false);
        }
        else                //----------
        {
            for (int i = 4; i < 8; i++)
            {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    public void onClickNextQuiz()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

        GameManager.instance.currentSelectedQuizNumber++;

        QuizDetail nextQuiz = GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber - 1];

        Debug.Log("Current quiz numer : " + GameManager.instance.currentSelectedQuizNumber + "   next quiz num : " + GameManager.instance.currentSelectedQuizNumber + "  next quiz ID : " + nextQuiz.Quiz_ID + " next quiz orerd : " + nextQuiz.Quiz_OrderID);

        StartCoroutine(NetworkManager.instance.getQuizQuestions(nextQuiz.Quiz_ID));
        GameManager.instance.resetGameData();
    }

    public void onClickHome()
    {
        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
        QuizAdvantureScreen.instance.ClearQ_AnswerLists();
    }
}
