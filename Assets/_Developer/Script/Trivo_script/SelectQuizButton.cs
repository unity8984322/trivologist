using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SelectQuizButton : MonoBehaviour
{
    public static Action<int> onSelectQuiz;
    public GameObject tickMarkImg, lockImg;
    public Image btnBG;
    public TextMeshProUGUI txtQuizNum;
    [SerializeField]
    private QuizDetail quizData;
    public string quizNimber;
    public bool isLocked;

    public void OnEnable()
    {
        //   isLocked = true;
    }

    public void setQuizData(QuizDetail quiz)
    {
        quizData = quiz;
    }

    public void onClickQuizBtn(/*QuizDetail quiz*/)
    {
        Debug.Log("CurrentQuiz NUmber = " + quizNimber);

        if (!isLocked)
        {
            UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            GameManager.instance.currentSelectedQuizNumber = int.Parse(quizNimber);
            StartCoroutine(NetworkManager.instance.getQuizQuestions(quizData.Quiz_ID.ToString()));

            //StartCoroutine(QuizAdvantureScreen.instance.enableDisablePowerupsPanel(true, 0.1f));            //------
        }
        else
        {
            QuizDetail previousQuiz;

            if (int.Parse(quizNimber) > 1)
                previousQuiz = GameManager.instance.allQuizes[int.Parse(quizNimber) - 2];
            else
                previousQuiz = GameManager.instance.allQuizes[int.Parse(quizNimber) - 1];

            Debug.Log("--------previousQuiz.Quiz_OrderID " + previousQuiz.Quiz_OrderID + "    ----   NetworkManager.instance.playerDetails.completeQuiz   : " + NetworkManager.instance.playerDetails.completeQuiz + "    ----IS able to unlock this quiz : " + (int.Parse(previousQuiz.Quiz_OrderID) < NetworkManager.instance.playerDetails.completeQuiz));

            //belowed condition : The quiz that the user clicked on checks whether the previous quiz is locked or not.
            if (int.Parse(previousQuiz.Quiz_OrderID) < NetworkManager.instance.playerDetails.completeQuiz           //--from <= to <
                || NetworkManager.instance.playerDetails.completeQuiz == 0)
            {

                Debug.Log("----Previous Quiz Order ID = " + previousQuiz.Quiz_OrderID + "-----completed quiz id = " + NetworkManager.instance.playerDetails.completeQuiz);

                AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("This quiz is locked, Do you want to unlock it by " + Necessary_data.coinsForUnlockQuiz + " coins ?",
                    () =>
                    {
                        if (Necessary_data.isCoinsAvailable(Necessary_data.coinsForUnlockQuiz))
                        {
                            Debug.Log("----try to unlock " + GameManager.instance.allQuizes[int.Parse(quizNimber) - 1].Quiz_Tite + " \n " + GameManager.instance.allQuizes[int.Parse(quizNimber) - 1].Quiz_ID + " \n " + GameManager.instance.allQuizes[int.Parse(quizNimber) - 1].Quiz_OrderID);

                            Necessary_data.subCoin(Necessary_data.coinsForUnlockQuiz);
                            StartCoroutine(NetworkManager.instance.unloackNextQuiz(Necessary_data.coinsForUnlockQuiz, int.Parse(quizNimber) - 1));
                        }
                        else
                        {
                            AllOtherAccess.instance.showPopupWith_Ok_Action("You don't have enough coins", null);
                        }
                    }
                    , null);
            }
            else
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("You can unlock quizzes sequentially only", null);
            }
        }
    }
}
