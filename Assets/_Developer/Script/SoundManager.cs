using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public AudioSource BG_audioSource, soundAudioSource;
    public AudioClip btnClick, timeOver, playerTurn;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);

        if (!PlayerPrefs.HasKey("init"))
        {
            PlayerPrefs.SetInt("init", 1);

            Necessary_data.isSoundOn = true;
            Necessary_data.isMusicOn = true;
        }


        setMusincSetting();
        playBGSound();
    }

    public void setMusincSetting()
    {
        if (Necessary_data.isMusicOn)
        {
            BG_audioSource.volume = 1f;
        }
        else
        {
            BG_audioSource.volume = 0;
        }

        if (Necessary_data.isSoundOn)
        {
            soundAudioSource.volume = 1f;
        }
        else
        {
            soundAudioSource.volume = 0;
        }

    }


    public void OnClickMusic(Action action)
    {
        if (Necessary_data.isMusicOn)
        {
            Necessary_data.isMusicOn = false;
            BG_audioSource.volume = 0;
        }
        else
        {
            Necessary_data.isMusicOn = true;
            BG_audioSource.volume = 1f;
        }

        playBGSound();
        playBtnClickSound();
        action.Invoke();
    }

    public void OnClickSound(Action action)
    {
        if (Necessary_data.isSoundOn)
        {
            Necessary_data.isSoundOn = false;
            soundAudioSource.volume = 0;
        }
        else
        {
            Necessary_data.isSoundOn = true;
            soundAudioSource.volume = 1f;
        }
        playBtnClickSound();
        action.Invoke();
    }

    public void playBGSound()
    {
        if (Necessary_data.isMusicOn)
            BG_audioSource.Play();
    }

    public void playBtnClickSound()
    {
        if (Necessary_data.isSoundOn)
            soundAudioSource.PlayOneShot(btnClick);
    }

    public void playPLayerTurnSound()
    {
        if (Necessary_data.isSoundOn)
            soundAudioSource.PlayOneShot(playerTurn);
    }

    public void playGameTimeOverSound()
    {
        if (Necessary_data.isSoundOn)
            soundAudioSource.PlayOneShot(timeOver);
    }
}
