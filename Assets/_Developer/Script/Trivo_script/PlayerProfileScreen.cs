using System;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using SimpleJSON;

public class PlayerProfileScreen : MonoBehaviour
{
    public static PlayerProfileScreen instance;

    [SerializeField]
    private RawImage profPic_Raw, avatarScreen_Profile_Raw;

    [SerializeField]
    private TextMeshProUGUI txtPlayerName;

    [SerializeField]
    private TextMeshProUGUI txtCoins;

    [SerializeField]
    private TextMeshProUGUI txtTotQues, txtCorrectAms, txtWinPer;

    public Button changeAvatarBtn;

    public static Action<SimpleJSON.JSONNode> OnProfileStatasticsFetched;

    public List<GameObject> otherAvatarsList;

    public static bool clickedOnSetNewAvatar;

    GameObject clickedAvatar;
    Sprite sp; // sprite to be passed to make texture

    public void Awake()
    {
        instance = this;
        clickedOnSetNewAvatar = false;
    }

    public void OnEnable()
    {
        StartCoroutine(NetworkManager.instance.getPlayerStatistics(Necessary_data.PlayerID));
        setCoins();
        setPLayerName();
        OnProfileStatasticsFetched += setStatasticsData;

        if (Necessary_data.LoginType != "guest")
            changeAvatarBtn.gameObject.SetActive(false);
        else
            changeAvatarBtn.gameObject.SetActive(true);
    }


    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoins;
        GameManager.OnProfPicSuccessfullyFetched += setProfilePicture;
    }



    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoins;
        GameManager.OnProfPicSuccessfullyFetched -= setProfilePicture;
    }



    public void OnDisable()
    {
        clickedOnSetNewAvatar = false;
        OnProfileStatasticsFetched -= setStatasticsData;
    }

    public void onClickBack(string panelName)
    {
        if (panelName == "PlayerProfile")
        {
            UIManager.instance.hideScreenOnly(UIScreens_ENM.PlayerProfile);

        }
        else if (panelName == "avatarPanel")
        {
            UIManager.instance.hideScreenOnly(UIScreens_ENM.avatarPanel);
            foreach (GameObject gb in otherAvatarsList)
            {
                if (otherAvatarsList.IndexOf(gb).ToString() == Necessary_data.PlayerAvtarID)
                {
                    setProfilePicture(gb.GetComponent<Image>().sprite.texture);       //-------
                    gb.transform.GetChild(0).gameObject.SetActive(true);
                }
                else gb.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    public void onClickShop()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.Shop);
    }

    public void onClickChangeAvatar()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.avatarPanel);
    }

    public void onClickLogout()
    {
        AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure want to logout?",
            () =>
            {
                AllOtherAccess.instance.logOutPlayer();
            }, null);
        GameManager.instance.allQuizes.Clear();         //-------
    }


    //--------
    public void OnClickOtherAvatars(Image otherAvatarSprite)
    {
        clickedOnSetNewAvatar = true;

        sp = otherAvatarSprite.sprite;
        clickedAvatar = otherAvatarSprite.gameObject;
        PlateformLoginManager.instance.getAvtarAsTexture(sp);
        //Debug.Log("Index of passed otherAvatarSprite = " + otherAvatarsList.IndexOf(otherAvatarSprite.gameObject));
        foreach (GameObject gb in otherAvatarsList)
        {
            if (sp.name == gb.GetComponent<Image>().sprite.name)
                gb.transform.GetChild(0).gameObject.SetActive(true);
            else
                gb.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    //Recheck the below code tomorrow-----------------------------------------------------------------------------------------
    public void OnClickDoneBtn()
    {
        {
            if (!string.IsNullOrEmpty(Necessary_data.PlayerAvtarID) && !string.IsNullOrWhiteSpace(Necessary_data.PlayerAvtarID) && Necessary_data.PlayerAvtarID != "")
            {
                clickedOnSetNewAvatar = true;
                PlateformLoginManager.instance.getAvtarAsTexture(sp);
                Necessary_data.PlayerAvtarID = "" + otherAvatarsList.IndexOf(clickedAvatar);
                StartCoroutine(PlateformLoginManager.instance.uploadPlayerAvtar(Necessary_data.PlayerID, Necessary_data.PlayerAvtarID));
            }
        }
    }

    public void setProfilePicture(Texture2D texture2)
    {
        //Debug.Log("------Prof Pic texture set in Profile Screen");
        profPic_Raw.texture = texture2;
        avatarScreen_Profile_Raw.texture = texture2;
        HomeScreen.instance.setProfilePicture(texture2);        //-------

    }

    private void setPLayerName()
    {
        txtPlayerName.text = Necessary_data.PlayerName;
    }

    private void setCoins()
    {
        txtCoins.text = Necessary_data.PlayerCoins.ToString();
    }

    private void setStatasticsData(SimpleJSON.JSONNode js)
    {
        txtTotQues.text = "Total Number Of Questions Answered : " + js["data"]["allTotal"];
        txtCorrectAms.text = "Number Of Questions Answered Correct: " + js["data"]["allCorrect"];
        // txtWinPer.text = winPer;
    }
}
