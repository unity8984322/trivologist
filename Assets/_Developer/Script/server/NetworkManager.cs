using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
//using BestHTTP.SocketIO;
//using LitJson;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    public bool wantToEnableFastLogin;
    //static bool isquizUnlocked;        //--------

    public GameObject reporter;

    public PlayerDetails playerDetails;



    public const string URL = "http://trivologist.com/api/";
    public const string URL_secure = "https://trivologist.com/api/";


    /// <summary>
    ///
    /// FOR SOCIAL LOGIN
    /// POST -- >> https://trivologist.com/api/Login
    /// Pass JSON
    /// {
    /// "id":"867780744319725",
    /// "name":"Krunal",
    /// "email":"krunal.vasundhara@gmail.com"
    /// }
    /// </summary>
    public const string socilaLogin = URL_secure + "Login";




    /// <summary>
    /// FOR GUEST LOGIN
    /// POST  ---- >>https://trivologist.com/api/Loginwithemail
    /// Pass JSON
    /// {
    //"email":"vasu2@gmail.com",
    //"password":"Vasu@123"
    //}
    /// </summary>
    public const string LoginWithGmail = URL_secure + "Loginwithemail";




    /// <summary>
    /// REGISTER GUEST USER
    /// GET  --->>> https://trivologist.com/api/Register     
    /// Pass JSON
    ///{
    //"email":"vasu2@gmail.com",
    //"password":"Vasu@123",
    //"source":"Email",
    //"name":"vasu2"
    //}
    /// </summary>
    public const string SignUp = URL_secure + "Register";




    /// <summary>
    /// FOR UPDATE PLAYER AVTAR
    /// GET  --->>>>https://trivologist.com/api/UpdateUserImage?userID=a45810a0-727f-11eb-8858-a7e148fd3710&imageUrl=http://graph.facebook.com/10156968700467411    
    /// </summary>
    public static readonly string _updatePlayerAvtar = URL_secure + "UpdateUserImage?userID=";



    /// <summary>
    /// FOR FIND USER BY EMAIL
    /// GET   --->>> https://trivologist.com/api/GetUser?email=vasu2@gmail.com
    /// Combine User Email ID
    /// </summary>
    public static readonly string GetUserByEmail = URL_secure + "GetUser?email=";//Email




    /// <summary>
    /// FOR USER STATISTICS
    /// GET   --->>  https://trivologist.com/api/GetUserStats?id=1e06c0c0-cd20-11ed-8c3b-eb28da868448
    /// Combine User ID with URL
    /// </summary>
    public static readonly string _getPlayerStatistics = URL_secure + "GetUserStats?id=";//set user id


    /// <summary>
    /// FOR SUBMIT USER ANSWER
    /// POST  ---- >>   https://trivologist.com/api/AddUserScoreQuestion (JSON)
    /// Pass with JSON
    /// {"UserID":"867780744319725","QuestionID":"0F3CD630-70DD-11EA-8267-81A5310DD57B","Score":true,"Time":"2"}
    /// </summary>
    public static readonly string sendPLayerScore = URL_secure + "AddUserScoreQuestion";


    /// <summary>
    /// GET QUESTION BY DATE
    ///GET - http://trivologist.com/api/GetQuestion?date=03/29/2023     
    /// </summary>
    /// 
    public static readonly string _getQuestionByDate = URL + "GetQuestion?date=";


    /// <summary>
    /// Pass Quiz ID get all questions of that quiz
    ///GET - http://trivologist.com/api/GetQuizQuestions/3F2E4C00-2F19-11ED-A748-1B8B4485E109
    /// </summary>
    public static readonly string _getAdvantureQuizQuestions = URL + "GetQuizQuestions/";

    /// <summary>
    /// Get Quiz that are on server
    ///GET - https://trivologist.com/api/GetQuizByOrder/1
    /// </summary>
    public static readonly string _getQuiz = URL + "GetQuizByOrder/";

    /// <summary>
    /// Get All quiz stored on server
    /// https://trivologist.com/api/GetQuizzes
    /// </summary>
    public static readonly string _getAllQuiz = URL + "GetQuizzes";


    /// <summary>
    /// 
    /// GET type
    /// ///https://trivologist.com/api/GetUserQuizDetails\n
    /// get data like how many quiz complete etc.
    /// {
    ///"status": 1,
    ///"message": "Successful",
    ///"data": [
    ///    {
    ///        "TotalQuizzes": 6,
    ///        "LastQuiz": "6",
    ///        "NextQuiz": 7,
    ///        "UserName": "Mark Sheehan",
    ///        "NextQuizID": "A79382C0-8D94-11EA-A814-672267BF95CC"
    ///}
    /// ]
    ///}
    /// </summary>
    public static readonly string _getUserQuizDetail = URL_secure + "GetUserQuizDetails";


    /// <summary>
    /// Unlock next quiz
    /// </summary>
    public static readonly string _unlockNextQuiz = URL_secure + "UnlockNextQuiz";



    /// <summary>
    /// Get calendar data
    /// </summary>
    public static readonly string _getCalenderData = URL_secure + "GetUserQuestionDetailsFromDates";

    /// <summary>
    ///{"userID":"1",
    ///"coinsSpent":10,
    ///"addedDoubleGuessCount":1000,
    ///"addedFreeAnswerCount":1005,
    ///"addedRemoveAnswerCount":1430
    ///}
    ///GET
    /// https://trivologist.com/api/BuyPowerUp
    /// </summary>
    public static readonly string _buyPowerUp = URL_secure + "BuyPowerUp";


    /// <summary>
    /// {
    /// "userID":"1",
    /// "coins":10
    /// }
    /// https://trivologist.com/api/BuyCoins
    /// </summary>
    public static readonly string _buyCoin = URL_secure + "BuyCoins";


    /// <summary>
    /// {
    ///     "userID":"1",
    ///     "powerUpUsed":"DoubleGuess"|"FreeAnswer"|"RemoveAnswer"
    /// }
    /// https://trivologist.com/api/UsePowerUp
    /// </summary>
    public static readonly string _usePowerUp = URL_secure + "UsePowerUp";


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            if (URL == "http://64.227.136.61:1616/")
            {
                wantToEnableFastLogin = false;
                reporter.SetActive(false);
            }
            else
            {
                reporter.SetActive(true);
            }

            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);

    }

    private void OnEnable()
    {
        //isquizUnlocked = true;
    }

    public IEnumerator getQuestionOfGivenDate(DateTime date, bool isTodaysDateQuistion)
    {
        string _date = Necessary_data.dateIn_MM_DD_YYYY(date);
        string query = _getQuestionByDate + _date;
        Debug.Log("----->>> Request For ------> Get Question By date : " + query);

        UnityWebRequest quesReq = UnityWebRequest.Get(query);

        //UnityWebRequest quesReq = new UnityWebRequest();
        //quesReq = UnityWebRequest.Put(query, "");
        //quesReq.method = UnityWebRequest.kHttpVerbGET;
        //quesReq.SetRequestHeader("Content-Type", "application/json");
        //quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("----->>> Response With Error ------> Get Question By date");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("----->>> Response Success ---->> Get Question By date ------> " + response);

            JSONNode results = JSONNode.Parse(response);

            QuestionDetail quizOfTheDay = new QuestionDetail();

            if (results.HasKey("status") && results["status"] == 1)
            {

                quizOfTheDay.q_Id = results["data"][0]["id"];
                quizOfTheDay.q_Question = results["data"][0]["Question"];
                quizOfTheDay.q_RightAns = results["data"][0]["Answer"];
                quizOfTheDay.q_WrongAns1 = results["data"][0]["Wronganswer1"];
                quizOfTheDay.q_WrongAns2 = results["data"][0]["Wronganswer2"];
                quizOfTheDay.q_WrongAns3 = results["data"][0]["Wronganswer3"];


                //quizOfTheDay.q_MoreInfo = Necessary_data.RemoveHTML(results["data"][0]["Moreinfo"].ToString());

                quizOfTheDay.q_MoreInfo = ParserCustom(Necessary_data.RemoveHTML(results["data"][0]["Moreinfo"].ToString()));

                GameManager.instance.quizOfTheDay = quizOfTheDay;

                Debug.Log("----->>> Data store success in class ---->> Get Question By date");
                QuesOfDayScreen.OnQuestionOfGivenDateIsReceived?.Invoke(date, true, isTodaysDateQuistion);
            }
            else
            {
                Debug.Log("----->>> Data store Not Not Not success in class ---->> Get Question By date");
                QuesOfDayScreen.OnQuestionOfGivenDateIsReceived?.Invoke(date, false, isTodaysDateQuistion);
            }
        }
    }

    private string ParserCustom(string str)
    {
        for (int i = 0; i < str.Length; i++)
        {
            if (str[i].ToString().Equals(">"))
            {
                if (str[i - 1].ToString().Equals("/"))
                {
                    str = str.Remove(i - 5, 6);
                }
            }
        }
        return str;
    }

    public IEnumerator getQuizQuestions(string quiz_id)
    {
        string query = _getAdvantureQuizQuestions + quiz_id;

        Debug.Log("----->>> Request For ------> Get Advanture Questions : " + query);
        UnityWebRequest quesReq = UnityWebRequest.Get(query);

        //UnityWebRequest quesReq = new UnityWebRequest();
        //quesReq = UnityWebRequest.Put(query, "");
        //quesReq.method = UnityWebRequest.kHttpVerbGET;
        //quesReq.SetRequestHeader("Content-Type", "application/json");
        //quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("----->>> Response With Error ------> Get Advanture Questions");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Get Advanture Questions ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            if (results.HasKey("status") && results["status"] == 1)
            {
                GameManager.instance.currentGameQuiz.quizAdvanture = new List<QuestionDetail>();
                for (int i = 0; i < results["data"].Count; i++)
                {
                    QuestionDetail advantureQuizQuistion = new QuestionDetail();
                    advantureQuizQuistion.q_Id = results["data"][i]["id"];
                    advantureQuizQuistion.q_Question = results["data"][i]["Question"];
                    advantureQuizQuistion.q_RightAns = results["data"][i]["Answer"];
                    advantureQuizQuistion.q_WrongAns1 = results["data"][i]["Wronganswer1"];
                    advantureQuizQuistion.q_WrongAns2 = results["data"][i]["Wronganswer2"];
                    advantureQuizQuistion.q_WrongAns3 = results["data"][i]["Wronganswer3"];
                    advantureQuizQuistion.q_MoreInfo = Necessary_data.RemoveHTML(results["data"][i]["Moreinfo"].ToString());

                    GameManager.instance.currentGameQuiz.quizAdvanture.Add(advantureQuizQuistion);
                }
                Debug.Log("----->>> Data store success in class ---->> Get Advanture Total Questions : " + results.Count);
                GameManager.instance.currentGameQuiz.totalQuestion = results["data"].Count;
                // UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdventure_selection);
                UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdvantureAScreen);


                GameManager.OnQdavantureQuizReceived?.Invoke();
            }
            else
            {
                Debug.Log("----->>> Data store Not Not Not success in class ---->> Get Advanture Questions");
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found. Please try later!", () => UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home));
            }
        }
    }

    public IEnumerator getQuizBy_ID(string quizNumber, bool isLastQuiz)         //not used in the game
    {
        string query = _getQuiz + quizNumber;

        Debug.Log("----->>> Request For ------> Get Quiz : " + query);

        UnityWebRequest quesReq = new UnityWebRequest();
        quesReq = UnityWebRequest.Put(query, "");
        quesReq.method = UnityWebRequest.kHttpVerbGET;
        quesReq.SetRequestHeader("Content-Type", "application/json");
        quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("----->>> Response With Error ------> Get Quiz ");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Get Quiz ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            if (results.HasKey("status") && results["status"] == 1)
            {

                QuizDetail quiz = new QuizDetail();

                quiz.Quiz_ID = results["data"]["id"];
                quiz.Quiz_Tite = results["data"]["Title"];
                quiz.Quiz_Description = results["data"]["Description"];
                quiz.isCompleted = false;
                GameManager.instance.allQuizes.Add(quiz);

                if (isLastQuiz)
                {
                    yield return new WaitForSeconds(2);
                    UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
                }

                //UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdvantureAScreen);

                //GameManager.OnQdavantureQuizReceived?.Invoke();
            }
            else
            {
                Debug.Log("----->>> Data store Not Not Not success in class ---->> Get Advanture Questions");
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found. Please try later!", () => UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home));
            }
        }
    }

    public IEnumerator getUserQuizLockUnlockDetails()
    {
        JSONObject json = new JSONObject
        {
            ["userID"] = Necessary_data.PlayerID
        };

        Debug.Log("----->>> Request For ------> getUserQuizLockUnlockDetails : " + _getUserQuizDetail + "     ---JSON : " + json.ToString());


        //Below code imported from stackoverflow
        //UnityWebRequest quesReq = UnityWebRequest.Get(_getUserQuizDetail);

        //    WebRequest request = WebRequest.Create(get.AbsoluteUri + args);
        //    request.Method = "GET";
        //    using (WebResponse response = request.GetResponse())
        //    {
        //        using (Stream stream = response.GetResponseStream())
        //        {
        //            XmlTextReader reader = new XmlTextReader(stream);
        //            ...
        //}
        //    }

        Debug.Log("-----------------Lock Unlock response");

        UnityWebRequest quesReq = new UnityWebRequest();

        quesReq = UnityWebRequest.Put(_getUserQuizDetail, json.ToString());
        quesReq.method = UnityWebRequest.kHttpVerbGET;
        quesReq.SetRequestHeader("Content-Type", "application/json");
        quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        //var text = quesReq.downloadHandler.text;        //---
        //Debug.Log("<color=black>" + quesReq + "</color>");                             //---

        Debug.Log("-----------------Lock Unlock response : " + quesReq.isDone + "    : " + quesReq.error);


        if (quesReq.result == UnityWebRequest.Result.ConnectionError)
        {
            print("----->>> Response With Error ------> Get Quiz ");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("Current Player's userID => " + json["userID"].ToString());
            Debug.Log("----->>> Response Sussess ------> Get Quiz Lock unlock data ----> " + response);

            JSONNode results = JSONNode.Parse(response);


            Debug.Log("<color=red>Data inside results = </color>" + results.ToString());

            if (results.HasKey("status") && results["status"] == 1)
            {
                //    playerDetails.TotalQuiz = results["data"][0]["TotalQuizzes"];
                playerDetails.completeQuiz = results["data"][0]["TotalQuizzes"];  //If client made changes in the API make changes here too  27-06-23--results["data"]["recordsets"][0][0]["TotalQuizzes"]; removed some part
                if (playerDetails.completeQuiz == 0)
                {
                    playerDetails.completeQuiz = 1;
                }
                UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            }
            else
            {
                Debug.Log("----->>> Issue on getting quiz Lock UnLock detail------");
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found. Please try later!", () => UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home));
            }
        }
    }


    public IEnumerator unloackNextQuiz(int coin, int quizNum)
    {
        string quizID = GameManager.instance.allQuizes[quizNum].Quiz_ID;

        Debug.Log("-- " + quizNum + "  --->>> Request For ------> Unlock Quiz manualy -:- next quiz is : " + GameManager.instance.allQuizes[quizNum].Quiz_Tite + " \n URL  :  " + _unlockNextQuiz);

        JSONObject json = new JSONObject
        {
            ["userID"] = Necessary_data.PlayerID,
            ["coinsSpent"] = coin,
            ["quizID"] = quizID
        };

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("clientSecret", "987");
        headers.Add("key", "123");

        WWWForm formData = new WWWForm();
        formData.AddField("userID", Necessary_data.PlayerID);
        formData.AddField("coinsSpent", coin);
        formData.AddField("quizID", quizID);

        Debug.Log("----->>> Request For ------> Unlock next quiz : " + _unlockNextQuiz + " Parameter : " + json.ToString());

        WWW quesReq = new WWW(_unlockNextQuiz, formData.data, headers);
        yield return quesReq;

        if (quesReq.error != null)
        {
            Debug.Log("------------Error on getting buyPowerup API data");
        }
        else
        {
            //----------
            string response = quesReq.text;
            Debug.Log("----->>> Response Sussess ------> Unlock Quiz manualy ----> " + response + " LENGTH = " + response.Length);

            JSONNode results = JSONNode.Parse(response + " results LENGTH = ");

            Debug.Log("Value of results[status] = " + results["status"]);
            if (results["status"] == 1)
            {
                Necessary_data.PlayerCoins = Necessary_data.PlayerCoins;// results["data"]["coins"];
                playerDetails.completeQuiz = results["data"]["TotalQuizzes"];
                if (playerDetails.completeQuiz == 0)
                {
                    playerDetails.completeQuiz = 1;
                }
                QuizListScreen.OnUnlockNextQuiz?.Invoke();
            }
            else
                Debug.Log("IN the ELSE part -> results[\"status\"] = " + results["status"]);
        }
    }

    private static readonly Regex sWhitespace = new Regex(@"\s+");

    public static string ReplaceWhitespace(string input, string replacement)
    {
        return sWhitespace.Replace(input, replacement);
    }

    [Obsolete]
    public IEnumerator getAllQuiz()
    {
        Debug.Log("----->>> Request For ------> Get All Quiz : " + _getAllQuiz);

        UnityWebRequest quesReq = UnityWebRequest.Get(_getAllQuiz);

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("----->>> Response With Error ------> Get Quiz ");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Get Quiz ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            if (results.HasKey("status") && results["status"] == 1)
            {
                for (int i = 0; i < results["data"].Count; i++)
                {
                    QuizDetail quiz = new QuizDetail();

                    quiz.Quiz_ID = results["data"][i]["id"];
                    quiz.Quiz_Tite = results["data"][i]["Title"];

                    string odId = results["data"][i]["QuizOrder"].ToString();

                    odId = ReplaceWhitespace(odId, string.Empty);
                    odId = odId.Replace("\"", string.Empty);
                    //Debug.Log(" title : " + quiz.Quiz_Tite + "    order : " + odId);

                    if (string.IsNullOrEmpty(odId) || string.IsNullOrWhiteSpace(odId) || odId == null || odId.ToString().Contains("null"))
                    {
                        //Debug.Log("Order id = null ");
                        odId = "0";
                    }
                    //odId = odId.Trim();
                    //Debug.Log("New order Id = " + odId);


                    quiz.Quiz_OrderID = odId;
                    quiz.Quiz_Description = results["data"][i]["Description"];
                    quiz.isCompleted = false;
                    GameManager.instance.allQuizes.Add(quiz);
                }

                GameManager.instance.allQuizes.Sort(new Comparison<QuizDetail>((x, y) => int.Parse(x.Quiz_OrderID).CompareTo(int.Parse(y.Quiz_OrderID))));

                StartCoroutine(getUserQuizLockUnlockDetails());

                //below 2 methods work same
                StartCoroutine(method_1(DateTime.Now, DateTime.Now.AddDays(-1095)));
                //StartCoroutine(getSavedDatesFromAPI(DateTime.Now, DateTime.Now.AddDays(-1095)));      //can call this one instad of the above one

            }
            else
            {
                Debug.Log("----->>> Data store Not Not Not success in class ---->> Get All Quiz");
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found. Please try later!", () => UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home));
            }
        }
    }

    [Obsolete]
    IEnumerator method_1(DateTime currentDate, DateTime lastDate)               //working on android too
    {

        JSONObject json = new JSONObject
        {
            ["userID"] = Necessary_data.PlayerID,
            ["endDate"] = Necessary_data.dateIn_MM_DD_YYYY(currentDate),
            ["startDate"] = Necessary_data.dateIn_MM_DD_YYYY(lastDate)
        };

        Debug.Log("<color=green>----Reques from method 1 </color>" + json.ToString());

        UnityWebRequest quesReq = new UnityWebRequest();
        //Debug.Log(json["userID"]);
        quesReq = UnityWebRequest.Put(_getCalenderData, json.ToString());
        quesReq.method = UnityWebRequest.kHttpVerbGET;
        quesReq.SetRequestHeader("Content-Type", "application/json");
        quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("<color=green>Res  -------111111 --- Error </color>" + quesReq.error);
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("<color=green>----->>>Calander Dates Attempted Response 11111 ----> </color>" + response);
            JSONNode qesReq_Response = JSONNode.Parse(response);

            if (qesReq_Response["status"] == 1)
            {
                GameManager.instance.calanderQuizdata.endingDate = lastDate;
                GameManager.instance.calanderQuizdata.startingDate = currentDate;

                for (int i = 0; i < qesReq_Response["data"]["recordset"].Count; i++)
                {
                    calanderAPIdata calanderData = new calanderAPIdata();

                    string date = qesReq_Response["data"]["recordset"][i]["DateUsed"].ToString().Replace("\"", string.Empty);

                    DateTime dtt = DateTime.Parse(date);

                    calanderData._date = Necessary_data.dateIn_MM_DD_YYYY(dtt);
                    calanderData.date = dtt;
                    //Debug.Log("----concat date  : " + dtt);

                    calanderData.isCorrectAns = qesReq_Response["data"]["recordset"][i]["Score"].ToString().Contains("true") ? true : false;
                    GameManager.instance.calanderQuizdata.calanderAPIdatelist.Add(calanderData);
                }
                Debug.Log("================End calander response deserializing");
            }
        }
    }


    //you can use this method also instead of method_1()
    [Obsolete]
    public IEnumerator getSavedDatesFromAPI(DateTime currentDate, DateTime lastDate)        //Working in android too
    {

        JSONObject json = new JSONObject
        {
            ["userID"] = Necessary_data.PlayerID,
            ["endDate"] = Necessary_data.dateIn_MM_DD_YYYY(currentDate),
            ["startDate"] = Necessary_data.dateIn_MM_DD_YYYY(lastDate)
        };

        UnityWebRequest sendrequest_put = new UnityWebRequest();

        Debug.Log("<color=Red>--------- Value of json before sending the rqeuest = </color>" + json.ToString());

        sendrequest_put = UnityWebRequest.Put(_getCalenderData, json.ToString());
        sendrequest_put.method = UnityWebRequest.kHttpVerbGET;
        sendrequest_put.SetRequestHeader("Content-Type", "application/json");
        sendrequest_put.SetRequestHeader("Accept", "application/json");

        yield return sendrequest_put.SendWebRequest();

        if (sendrequest_put.isNetworkError)
        {
            print("<color=green>Res  -------111111 --- Error </color>" + sendrequest_put.error);
        }
        else
        {
            string response = sendrequest_put.downloadHandler.text;
            Debug.Log("<color=green>----->>> Response 11111 ----> </color>" + response);
            JSONNode qesReq_Response = JSONNode.Parse(response);

            if (qesReq_Response["status"] == 1)
            {
                GameManager.instance.calanderQuizdata.endingDate = lastDate;
                GameManager.instance.calanderQuizdata.startingDate = currentDate;

                for (int i = 0; i < qesReq_Response["data"]["recordset"].Count; i++)
                {
                    calanderAPIdata calanderData = new calanderAPIdata();

                    string date = qesReq_Response["data"]["recordset"][i]["DateUsed"].ToString().Replace("\"", string.Empty);

                    DateTime dtt = DateTime.Parse(date);

                    calanderData._date = Necessary_data.dateIn_MM_DD_YYYY(dtt);
                    calanderData.date = dtt;
                    Debug.Log("----concat date  : " + dtt);

                    calanderData.isCorrectAns = qesReq_Response["data"]["recordset"][i]["Score"].ToString().Contains("true") ? true : false;
                    GameManager.instance.calanderQuizdata.calanderAPIdatelist.Add(calanderData);
                }
                Debug.Log("================End calander response deserializing");
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public async System.Threading.Tasks.Task SendPlayerAnswerAsync(string player_Id, string question_Id, string score, string time, bool isQuizAdvanture)
    {
        JSONObject json = new JSONObject
        {
            ["UserID"] = player_Id,
            ["QuestionID"] = question_Id,
            ["Score"] = score,
            ["Time"] = time
        };

        var client = new HttpClient();
        var request = new HttpRequestMessage(HttpMethod.Post, sendPLayerScore);
        //request.Headers.Add("Cookie", "ARRAffinity=51f7d50ebb3aaef6dc8dc5c230072f869db8a7342ae1196cc78e4b3b1ab2053e; ARRAffinitySameSite=51f7d50ebb3aaef6dc8dc5c230072f869db8a7342ae1196cc78e4b3b1ab2053e");
        //var content = new StringContent("{\"UserID\":\"1e06c0c0-cd20-11ed-8c3b-eb28da868448\",\"QuestionID\":\"14C291F0-05B1-11EE-9D42-67C8E6EC71CE\",\"Score\":true,\"Time\":\"10\"}", null, "application/json");
        request.Content = new StringContent(json);      //------
        var response = await client.SendAsync(request);
        response.EnsureSuccessStatusCode();

        //Console.WriteLine(await response.Content.ReadAsStringAsync());
        ///
        if (response.IsSuccessStatusCode == false)
        {
            print("----->>> Response With Error ------> Submit Player Answer" + response.ToString());
        }
        else
        {
            Debug.Log("----->>> Response Sussess ------> Submit Player Answer ----> " + response.ToString());

            string result = await response.Content.ReadAsStringAsync();

            JSONNode results = JSONNode.Parse(result);
            Debug.Log(results.ToString());

            if (isQuizAdvanture)
            {
                if (results.HasKey("status") && results["status"] == 1)
                {
                    if (results["AlreadyAnswered"] == false && score == "true")
                    {
                        StartCoroutine(buyCoins(json["UserID"], 10, "None"));
                    }                          //--------
                    else
                        Debug.Log("No Coins Added");

                    //user hasn't already answered this question so add coins
                    QuizAdvantureScreen.OnAnswerSendSuccess?.Invoke();
                    Debug.Log("---->On answer success");
                }
                else
                {
                    Debug.Log("---->On answer falied");
                    QuizAdvantureScreen.OnAnswerSendFailed?.Invoke();
                }
            }
            else
            {
                if (results.HasKey("status") && results["status"] == 1)
                {
                    if (results["AlreadyAnswered"] == false && score == "true")                          //--------
                        StartCoroutine(buyCoins(json["UserID"], 10, "None"));
                    else
                        Debug.Log("No Coins Added");

                    Debug.Log("---->On answer success");
                    QuesOfDayScreen.OnAnswerSendSuccess?.Invoke();
                }
                else
                {
                    Debug.Log("---->On answer falied");
                    QuesOfDayScreen.OnAnswerSendFailed?.Invoke();
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Not used in the game
    //public IEnumerator submitPlayerAnwser(string player_Id, string question_Id, bool score, string time, bool isQuizAdvanture)
    //{
    //    //QuizListScreen.OnUnlockNextQuiz += QuizListScreen.instance.unLockQuiz;       //------

    //    JSONObject json = new JSONObject
    //    {
    //        ["UserID"] = player_Id,
    //        ["QuestionID"] = question_Id,
    //        ["Score"] = score,
    //        ["Time"] = time
    //    };

    //    Debug.Log("----->>> Request For ------> Submit Player Answer : " + sendPLayerScore + " Parameter : " + json.ToString());

    //    UnityWebRequest quesReq = new UnityWebRequest();
    //    quesReq = UnityWebRequest.Put(sendPLayerScore, json);
    //    quesReq.method = UnityWebRequest.kHttpVerbPOST;
    //    quesReq.SetRequestHeader("Content-Type", "application/json");
    //    quesReq.SetRequestHeader("Accept", "application/json");

    //    yield return quesReq.SendWebRequest();

    //    if (quesReq.isNetworkError)
    //    {
    //        print("----->>> Response With Error ------> Submit Player Answer");
    //    }
    //    else
    //    {
    //        string response = quesReq.downloadHandler.text;
    //        Debug.Log("----->>> Response Sussess ------> Submit Player Answer ----> " + response);


    //        JSONNode results = JSONNode.Parse(response);

    //        if (isQuizAdvanture)
    //        {
    //            if (results.HasKey("status") && results["status"] == 1)
    //            {
    //                Debug.Log("---->On answer success");
    //                QuizAdvantureScreen.OnAnswerSendSuccess?.Invoke();
    //            }
    //            else
    //            {
    //                Debug.Log("---->On answer falied");
    //                QuizAdvantureScreen.OnAnswerSendFailed?.Invoke();
    //            }
    //        }
    //        else
    //        {
    //            if (results.HasKey("status") && results["status"] == 1)
    //            {
    //                Debug.Log("---->On answer success");
    //                QuesOfDayScreen.OnAnswerSendSuccess?.Invoke();
    //            }
    //            else
    //            {
    //                Debug.Log("---->On answer falied");
    //                QuesOfDayScreen.OnAnswerSendFailed?.Invoke();
    //            }
    //        }
    //    }
    //}

    public IEnumerator getPlayerStatistics(string player_id)
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

        string query = _getPlayerStatistics + player_id;

        Debug.Log("----->>> Request For ------> Get Player Statistics : " + query);

        string aa = "";


        UnityWebRequest quesReq = UnityWebRequest.Get(query);

        //UnityWebRequest quesReq = new UnityWebRequest();
        //quesReq = UnityWebRequest.Put(query, "");
        //quesReq.method = UnityWebRequest.kHttpVerbGET;
        //quesReq.SetRequestHeader("Content-Type", "application/json");
        //quesReq.SetRequestHeader("Accept", "application/json");

        yield return quesReq.SendWebRequest();

        if (quesReq.isNetworkError)
        {
            print("----->>> Response With Error ------> Get Player Statistics");
        }
        else
        {
            string response = quesReq.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Get Player Statistics ----> " + response);
            JSONNode results = JSONNode.Parse(response);
            if (results.HasKey("status") && results["status"] == 1)
            {
                PlayerProfileScreen.OnProfileStatasticsFetched?.Invoke(results);
            }
        }
        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
    }

    public IEnumerator buyPowerUps(string player_Id, int coin, int TwoGuess, int freeAns, int removeOption, UnityEngine.UI.Button buyBtns)          //powerup
    {
        JSONObject json = new JSONObject
        {
            ["userID"] = player_Id,
            ["coinsSpent"] = coin,
            ["addedDoubleGuessCount"] = TwoGuess,
            ["addedFreeAnswerCount"] = freeAns,
            ["addedRemoveAnswerCount"] = removeOption
        };


        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("clientSecret", "987");
        headers.Add("key", "123");

        WWWForm formData = new WWWForm();
        formData.AddField("userID", player_Id);
        formData.AddField("coinsSpent", coin);
        formData.AddField("addedDoubleGuessCount", TwoGuess);
        formData.AddField("addedFreeAnswerCount", freeAns);
        formData.AddField("addedRemoveAnswerCount", removeOption);


        Debug.Log("----->>> Request For ------> Submit Buy PowerUp : " + _buyPowerUp + " Parameter : " + json.ToString());

        WWW quesReq = new WWW(_buyPowerUp, formData.data, headers);
        yield return quesReq;


        buyBtns.interactable = true;


        if (quesReq.error != null)
        {
            Debug.Log("------------Error on getting buyPowerup API data");
        }
        else
        {
            string response = quesReq.text;
            Debug.Log("----->>> Response Sussess ------> Buy PowerUp ----> " + response);

            JSONNode results = JSONNode.Parse(response);


            //  AllOtherAccess.instance.showPopupWith_Ok_Action(results["Message"], null);
            if (results["status"] == 1)
            {
                Necessary_data.PlayerCoins = results["NewCoinCount"].AsInt;
                Necessary_data.PlayerPowerUp_0 = results["newDoubleGuessCount"].AsInt;
                Necessary_data.PlayerPowerUp_1 = results["newFreeAnswerCount"].AsInt;
                Necessary_data.PlayerPowerUp_2 = results["newRemoveAnswerCount"].AsInt;
                //GameManager.OnUpdateCoin?.Invoke();
                //GameManager.OnUpdatePowerup?.Invoke();
            }
        }
        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
    }

    public IEnumerator buyCoins(string player_Id, int coin, string msg = "None")
    {

        JSONObject json = new JSONObject
        {
            ["userID"] = player_Id,
            ["coins"] = coin
        };

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("clientSecret", "987");
        headers.Add("key", "123");

        WWWForm formData = new WWWForm();
        formData.AddField("userID", player_Id);
        formData.AddField("coins", coin);

        Debug.Log("----->>> Request For ------> Submit Buy Coins: " + _buyCoin + " Parameter : " + json.ToString()); ;
        Debug.Log("----Buy coin call for ; " + msg);
        //Debug.Log("Value of playerpref -- unlockedQuiz = " + PlayerPrefs.GetInt("unlockedQuiz"));
        //Debug.Log("Score of current quiz = " + QuizAdvantureScreen.instance.scoreOfCurrentQuiz);
        WWW quesReq = new WWW(_buyCoin, formData.data, headers);
        yield return quesReq;

        if (ShopPanel.instance != null)
        {
            foreach (var item in ShopPanel.instance.shopPanels)
            {
                item.buyBtn.interactable = true;
            }
        }
        if (quesReq.error != null)
        {
            print("----->>> Response With Error ------> Submit Player Answer");
        }
        else
        {
            string response = quesReq.text;
            Debug.Log("----->>> Response Sussess ------> Coin added on correct answer ----> " + response);

            JSONNode results = JSONNode.Parse(response);


            //   AllOtherAccess.instance.showPopupWith_Ok_Action(results["Message"], null);
            if (results["status"] == 1)
            {
                Necessary_data.PlayerCoins = results["NewCoinCount"].AsInt;
            }
        }
    }

    public IEnumerator usePowerUp(string player_Id, string powerUpname)         //powerup
    {
        Debug.Log("passed powerup = " + powerUpname);
        //(string) powerups --> DoubleGuess           FreeAnswer          RemoveAnswer

        JSONObject json = new JSONObject
        {
            ["userID"] = player_Id,
            ["powerUpUsed"] = powerUpname
        };

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("clientSecret", "987");
        headers.Add("key", "123");

        WWWForm formData = new WWWForm();
        formData.AddField("userID", player_Id);
        formData.AddField("powerUpUsed", powerUpname);


        Debug.Log("----->>> Request For ------> Submit Buy Coins: " + _usePowerUp + " Parameter : " + json.ToString());

        WWW quesReq = new WWW(_usePowerUp, formData.data, headers);
        yield return quesReq;

        if (quesReq.error != null)
        {
            print("----->>> Response With Error ------> Submit Player Answer" + quesReq.error);
        }
        else
        {
            string response = quesReq.text;
            Debug.Log("----->>> Response Sussess ------> Buy PowerUp ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            if (results["status"] == 1)
            {
                Necessary_data.PlayerPowerUp_0 = results["newDoubleGuessCount"].AsInt;
                Necessary_data.PlayerPowerUp_1 = results["newFreeAnswerCount"].AsInt;
                Necessary_data.PlayerPowerUp_2 = results["newRemoveAnswerCount"].AsInt;
            }
        }
    }
}
