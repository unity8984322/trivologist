using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickSound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Button>())
            GetComponent<Button>().onClick.AddListener(() => SoundManager.instance.playBtnClickSound());
    }
}
