using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpManager : MonoBehaviour
{
    public static PowerUpManager instance;

    [SerializeField]
    private TextMeshProUGUI[] txtPowerUPCount;

    public GameObject homeScreen, quizAdventureScreen;



    #region POWER UP PURCHASE PANEL         
    [Space(2)]                                  //powerup
    [Header("POWER UP PURCHASE PANEL Data")]
    [Space(2)]

    [SerializeField]
    public GameObject powerUpsBottomPanel;
    [SerializeField]
    public GameObject purchasePowerUPScreen;       //powerup
    [SerializeField]
    private Button powerUpBuyBtn;                  //powerup
    public int currentPowerUpIndex = 0;         //powerup
    public GameObject[] powerUpPaels;          //powerup
    public List<AllPowerUpsPanel> listOfAllPowerUpsPanel;

    [SerializeField]
    private TMPro.TextMeshProUGUI txtPrices;

    #endregion


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    private void OnEnable()
    {
        purchasePowerUPScreen.SetActive(false);
    }
    public void setPowerUp()               //powerup
    {
        txtPowerUPCount[0].text = Necessary_data.PlayerPowerUp_0 + "";
        txtPowerUPCount[2].text = Necessary_data.PlayerPowerUp_1 + "";
        txtPowerUPCount[1].text = Necessary_data.PlayerPowerUp_2 + "";
    }

    public void onClickPowerUp(int id)              //powerup --inspector
    {
        currentPowerUpIndex = id;
        //Debug.Log("value of currentPowerUpIndex = " + currentPowerUpIndex + "id passed to the function on btn click --> " + id);      //------
        whenClickPurchasePowerBtn();
    }

    public void whenClickPurchasePowerBtn()         //powerup
    {
        purchasePowerUPScreen.SetActive(true);

        foreach (var item in powerUpPaels)
        {
            item.SetActive(false);
        }

        txtPrices.text = GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost + "";

        powerUpPaels[currentPowerUpIndex].SetActive(true);
    }

    public void onClosePowerUpPanel()               //powerup  --inspector
    {

        //InvokeRepeating(nameof(QuizAdvantureScreen.instance.setQuestionTimer), 0, 1);
        if (quizAdventureScreen.activeInHierarchy)
            QuizAdvantureScreen.instance.InvokeRepeating(nameof(QuizAdvantureScreen.instance.setQuestionTimer), 0, 1);      //----
        currentPowerUpIndex = 0;
        purchasePowerUPScreen.SetActive(false);
    }


    public void onCLickPowerUpBuyBtn()
    {

        Debug.Log(Necessary_data.PlayerPowerUp_0 + "     -      " + Necessary_data.PlayerPowerUp_1 + "      --    " + Necessary_data.PlayerPowerUp_2);

        if (Necessary_data.isCoinsAvailable(GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost))
        {
            UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            Debug.Log("---------> currentPowerUpIndex" + currentPowerUpIndex);

            powerUpBuyBtn.interactable = false;
            switch (currentPowerUpIndex)
            {
                case 0:
                    StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
             0,
             0,
             powerUpBuyBtn));
                    break;

                case 1:
                    StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
             0,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
             0,
             powerUpBuyBtn));
                    break;
                case 2:
                    StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
             0,
             0,
             GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
             powerUpBuyBtn));
                    break;

                default:
                    Debug.Log("--------->Switch(Default) currentPowerUpIndex" + currentPowerUpIndex);
                    break;
            }
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have enough coins!\nDo you wants to buy more coins?",
                () =>
                {
                    UIManager.instance.showScreenOnly(UIScreens_ENM.Shop);
                    //  UIManager.instance.hideScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                }, null);
        }
    }
}

[Serializable]
public class AllPowerUpsPanel
{
    public string parentOfPowerUpsPanel;
    public GameObject powerUpPanel;
}
