using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using System.Globalization;

public enum UIScreens_ENM
{
    Splash,
    loginscreen,
    signupscreen,
    Home,
    LOADINGScreen,
    HowToPlay,
    Shop,
    PlayerProfile,
    QuizAdventure_selection,
    QuesOfDay,
    Calander,
    Result,
    QuizAdvantureAScreen,
    PurchasePowerUpScreen,
    QuizListScreen,
    settingScreen,
    avatarPanel

}


/// <summary>
///q_Id;
///q_Question;
///q_RightAns;
///q_WrongAns1;
///q_WrongAns2;
///q_WrongAns3; 
/// </summary>
[Serializable]
public class QuestionDetail
{
    public string q_Id;
    public string q_Question;
    public string q_RightAns;
    public string q_WrongAns1;
    public string q_WrongAns2;
    public string q_WrongAns3;
    public string q_MoreInfo;
}


[Serializable]
public class QuizDetail
{
    public static QuizDetail QuizDetailinstance;
    public string Quiz_Tite;
    public string Quiz_ID;
    public string Quiz_OrderID;
    public string Quiz_Description;
    public bool isCompleted;
}


[Serializable]
public class PlayerDetails
{
    public string player_ID;
    public string player_Name;
    public string player_Email;
    public string player_avtarID;
    public int player_Coin;
    public int player_Powerups_0;
    public int player_Powerups_1;
    public int player_Powerups_2;
    public string player_LoginType;
    public int player_historyDays;
    //public int TotalQuiz;
    public int completeQuiz;

}


public static class Necessary_data
{

    public const string loginScene = "Login_SignUp_Intro";
    public const string mainScene = "MainScene";

    public static char[] special = { '@', '#', '$', '%', '^', '&', '+', '=', '_', '.', '!', '`', '~', '*', ',', '?', '-' }; // or whatever  
    public const string info_InAppropriateUsername = "Username may contain inappropriate text , please enter a different username";
    public const string info_InvalidEmail = "Invalid email format";
    public const string info_EmailNotFound = "Email does not exist";
    public const string info_EmailExist = "Email exists, please enter a different email";
    public const string info_UsernameExist = "Username exists, please enter a different username";
    public const string info_PasswordNotStrong = "Password does not meet the requirement";
    public const string info_shortPassword = "Password need 8 character at least";

    public const string info_PasswordNotMatch = "Password does not match";



    public static System.Tuple<bool, string> isPasswordDoesNotMeetAllReq(string str)
    {
        if (str == "")
        {
            return new System.Tuple<bool, string>(true, "Please enter password");
        }
        else if (!isPasswordLengthValid(str))
        {
            return new System.Tuple<bool, string>(true, "Password need at least 8 characters");
        }
        else if (!str.Any(char.IsUpper))
        {
            return new System.Tuple<bool, string>(true, "Password need at least one capital letter");
        }
        else if (!str.Any(char.IsLower))
        {
            return new System.Tuple<bool, string>(true, "Password need at least one small letter");
        }
        else if (!str.Any(char.IsNumber))
        {
            return new System.Tuple<bool, string>(true, "Password need at least one numeric value");
        }
        else if (str.IndexOfAny(special) == -1)
        //else if (isStrongPassword(str))
        {
            return new System.Tuple<bool, string>(true, "Password need at least one special character");
        }
        else if (!isStrongPassword(str))
        {
            return new System.Tuple<bool, string>(true, "Password odes not meet all requirement");
        }
        else
        {
            return new System.Tuple<bool, string>(false, "Password meet all req");
        }
    }

    public static List<string> inAppropriateWords = new List<string> { "\n\n" };

    public static bool isItInappropiateWords(string str)
    {
        str = str.ToLower();
        return inAppropriateWords.Any(w => str.Contains(w));
    }


    public static string RemoveHTML(string text)
    {
        text = text.Replace("&nbsp;", " ")
            .Replace("<br>", "")
            .Replace("description", "")
            .Replace("INFRA:CORE:", "")
            .Replace("RESERVED", "")
            .Replace(":", "")
            .Replace(";", "")
            .Replace("-0/3/0", "");
        var oRegEx = new Regex("<[^>]+>");
        return oRegEx.Replace(text, string.Empty);
    }


    public static int coinValueOnCorrectAns = 10;
    public static int coinsForUnlockQuiz = 10;
    public static string supportEmailid = "helpandsuppor@gmail.com";

    public static string profilePicURL
    {
        get => PlayerPrefs.GetString("profPicUrlString", "");
        set => PlayerPrefs.SetString("profPicUrlString", value);
    }

    public static string ProfPicBaseString
    {
        get => PlayerPrefs.GetString("profPicBaseString", "");
        set => PlayerPrefs.SetString("profPicBaseString", value);
    }

    public static bool IsLoggedIn
    {
        get => PlayerPrefs.GetInt("LoggedIn", 0) != 0;
        set => PlayerPrefs.SetInt("LoggedIn", value ? 1 : 0);
    }

    public static string PlayerID
    {
        get => PlayerPrefs.GetString("PlayerID", "1");
        set
        {
            NetworkManager.instance.playerDetails.player_ID = value;
            PlayerPrefs.SetString("PlayerID", value);
        }
    }

    public static string PlayerName
    {
        get => PlayerPrefs.GetString("UserName", "XYZ");
        set
        {
            NetworkManager.instance.playerDetails.player_Name = value;
            PlayerPrefs.SetString("UserName", value);
        }
    }

    public static int PlayerCoins
    {
        get => PlayerPrefs.GetInt("coin", 0);
        set
        {
            NetworkManager.instance.playerDetails.player_Coin = value;
            PlayerPrefs.SetInt("coin", value);
            GameManager.OnUpdateCoin?.Invoke();
        }
    }


    /// <summary>
    /// Two guess
    /// </summary>
    public static int PlayerPowerUp_0
    {
        get => PlayerPrefs.GetInt("powerups_0", 0);
        set
        {
            NetworkManager.instance.playerDetails.player_Powerups_0 = value;
            PlayerPrefs.SetInt("powerups_0", value);
            GameManager.OnUpdatePowerup?.Invoke();
        }
    }

    /// <summary>
    /// Free answer
    /// </summary>
    public static int PlayerPowerUp_1
    {
        get => PlayerPrefs.GetInt("powerups_1", 0);
        set
        {
            NetworkManager.instance.playerDetails.player_Powerups_1 = value;
            PlayerPrefs.SetInt("powerups_1", value);
            GameManager.OnUpdatePowerup?.Invoke();
        }
    }

    /// <summary>
    /// Remove option
    /// </summary>
    public static int PlayerPowerUp_2
    {
        get => PlayerPrefs.GetInt("powerups_2", 0);
        set
        {
            NetworkManager.instance.playerDetails.player_Powerups_2 = value;
            PlayerPrefs.SetInt("powerups_2", value);
            GameManager.OnUpdatePowerup?.Invoke();
        }
    }



    public static string PlayerEmail
    {
        get => PlayerPrefs.GetString("pemail", "");
        set
        {
            NetworkManager.instance.playerDetails.player_Email = value;
            PlayerPrefs.SetString("pemail", value);
        }
    }

    public static string PlayerAvtarID
    {
        get => PlayerPrefs.GetString("avtarid", "1");
        set
        {
            PlayerPrefs.SetString("avtarid", value);
            NetworkManager.instance.playerDetails.player_avtarID = value;
        }
    }

    public static string Source
    {
        get => PlayerPrefs.GetString("source", "");
        set => PlayerPrefs.SetString("source", value);
    }


    public static string LoginType
    {
        get => PlayerPrefs.GetString("logintype", "");
        set
        {
            NetworkManager.instance.playerDetails.player_LoginType = value;
            PlayerPrefs.SetString("logintype", value);
        }
    }

    public static string isFirstLogin
    {
        get => PlayerPrefs.GetString("isfirstlogin", "");
        set => PlayerPrefs.SetString("isfirstlogin", value);
    }

    public static int playerHistoryDays
    {
        get => PlayerPrefs.GetInt("p_historydays", 7);
        set
        {
            NetworkManager.instance.playerDetails.player_historyDays = value;
            PlayerPrefs.SetInt("p_historydays", value);
        }
    }

    public static bool IsPlayerInLoginScene
    {
        get
        {
            if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == mainScene)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public static bool isSoundOn
    {
        get
        {
            if (PlayerPrefs.GetInt("sound", 0) == 1)
                return true;
            else
                return false;
        }
        set
        {
            if (value == true)
                PlayerPrefs.SetInt("sound", 1);
            else
                PlayerPrefs.SetInt("sound", 0);
        }
    }

    public static bool isMusicOn
    {
        get
        {
            if (PlayerPrefs.GetInt("music", 0) == 1)
                return true;
            else
                return false;
        }
        set
        {
            if (value == true)
                PlayerPrefs.SetInt("music", 1);
            else
                PlayerPrefs.SetInt("music", 0);
        }
    }


    public static bool IsInternetAccessUnavailable()
    {
        return Application.internetReachability == NetworkReachability.NotReachable;
    }


    public static bool isEmptyInputField(string str)
    {

        if (string.IsNullOrWhiteSpace(str))
        {
            Debug.Log("aaaa");
            return true;
        }
        else if (string.IsNullOrEmpty(str))
        {
            Debug.Log("fffff");
            return true;
        }

        if (str == "")
            return true;
        else
            return false;
    }

    public static bool isValidEmail(string email)
    {
        string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,6})$";

        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;

    }

    public static bool isStrongPassword(string str)
    {

        int validConditions = 0;
        foreach (char c in str)
        {
            if (c >= 'a' && c <= 'z')
            {
                validConditions++;
                break;
            }
        }
        if (validConditions == 0)
            return false;

        foreach (char c in str)
        {
            if (c >= 'A' && c <= 'Z')
            {
                validConditions++;
                break;
            }
        }

        if (validConditions == 1)
            return false;

        foreach (char c in str)
        {
            if (c >= '0' && c <= '9')
            {
                validConditions++;
                break;
            }
        }

        if (validConditions == 2)
            return false;


        if (str.IndexOfAny(special) == -1)
        {
            return false;
        }


        return true;

    }


    public static bool isPasswordLengthValid(string str)
    {
        if (str.Length >= 8)
            return true;
        else
            return false;
    }

    public static Texture GetTextureFromImageString(string _imageString)
    {
        var byteArrayPlayerProfileImage = Convert.FromBase64String(_imageString);
        var textureToLoadImage = new Texture2D(1, 1); ;
        textureToLoadImage.LoadImage(byteArrayPlayerProfileImage);
        return textureToLoadImage;
    }

    public static bool isStringNotNullOrEmpty(string str)
    {
        if (str != "null" && !String.IsNullOrEmpty(str) && str != null && str != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static string dateIn_MM_DD_YYYY(DateTime date)
    {
        return date.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);
    }


    public static DateTime dateIn_MM_DD_YYYY(string date)
    {
        return DateTime.Parse(date);
    }

    /*
     
    DateTime currentTime = DateTime.UtcNow;
    DateTime lastTimeClicked;

    //reseting the health every time button is clicked and saving the time of it
    public void buttonClicked()
    {
        health = 100;
        PlayerPrefs.SetString("Last Time Clicked", DateTime.UtcNow.ToString()); // you can add a formatter here if you want
    }

    public void Update()
    {

        //calculating the difference between last click and actual time

    lastTimeClicked = DateTime.Parse(PlayerPrefs.GetString("Last Time Clicked"));
        print("LastTimeClicked" + lastTimeClicked);

        TimeSpan difference = currentTime.Subtract(lastTimeClicked);
        print("Difference: " + difference);
    }
     
     
     */

    /// <summary>
    /// Method is used to get date by adding or substracting day from given date
    /// </summary>
    /// <param name="numOfDay"></param>
    /// <param name="_date"></param>
    /// <returns></returns>
    public static DateTime addDayInDate(int numOfDay, DateTime _date)
    {
        DateTime newDateis = _date.AddDays(numOfDay);
        return newDateis;
    }

    public static bool isGivenDateIsLaterThanToday(DateTime _date)
    {
        int result = DateTime.Compare(DateTime.Today, _date);

        Debug.Log("------>isGivenDateIsLaterThanToday :  " + result + " Today's Date : " + dateIn_MM_DD_YYYY(DateTime.Today) + "  Passed Date  " + dateIn_MM_DD_YYYY(_date));

        if (result < 0)
            return true;
        else
            return false;
    }

    public static bool isDateValidForHistory(DateTime _date, int numOfDayForHistory)
    {
        DateTime todayDate = DateTime.Today;

        DateTime lastDayOfHistory = addDayInDate(-numOfDayForHistory, todayDate);


        int result = DateTime.Compare(lastDayOfHistory, _date);

        Debug.Log("------>isDateValidForHistory :  " + result + " Today's Date : " + dateIn_MM_DD_YYYY(DateTime.Today) + "  Passed Date  " + dateIn_MM_DD_YYYY(_date) + "  History's last Date : " + dateIn_MM_DD_YYYY(lastDayOfHistory));

        if (result <= 0)
            return true;
        else
            return false;
    }

    public static bool isOutDate(DateTime date1, DateTime date2)
    {
        int result = DateTime.Compare(date1, date2);
        string relationship;

        if (result < 0)
            relationship = "is earlier than";
        else if (result == 0)
            relationship = "is the same time as";
        else
            relationship = "is later than";

        Console.WriteLine("{0} {1} {2}", date1, relationship, date2);
        return true;
    }


    public static bool isAnswerMatched(string userAns, string actualAns)
    {
        string _userAns = userAns.ToLower();
        _userAns = userAns.Trim();

        string _actualAns = actualAns.ToLower();
        _actualAns = actualAns.Trim();

        //Debug.Log("userAns given = " + userAns + "-----------ActualAns = " + actualAns);
        //Debug.Log(_userAns == _actualAns);
        return _userAns == _actualAns;
    }


    public static bool IsPointerOverItemUI(string tag = "")
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raysastResults);
        foreach (RaycastResult raysastResult in raysastResults)
        {
            //Enable below condidtion if you want to check tag wise
            if (raysastResult.gameObject.CompareTag(tag))
            {
                return true;
            }
        }
        return false;
    }


    public static string FirstCharToUpper(string input)
    {

        string firstLatter = input.Substring(0, 1);
        firstLatter = firstLatter.ToUpper();
        string restLatters = input.Substring(1);

        string finalString = firstLatter + restLatters;

        return finalString;
    }

    public static void subCoin(int valueToMinus)
    {
        PlayerCoins -= valueToMinus;
        GameManager.OnUpdateCoin?.Invoke();
    }


    public static bool isCoinsAvailable(int coinCost)
    {
        return (PlayerCoins >= coinCost);
    }
}

[Serializable]
public class UiScree_Cls
{
    public string nm;
    public UIScreens_ENM UiScreens_ENM;
    public GameObject screen;

}
