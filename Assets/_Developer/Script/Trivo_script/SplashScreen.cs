using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

namespace trivologist
{
    public class SplashScreen : MonoBehaviour
    {
        public static SplashScreen instance;
        [SerializeField]
        private Transform loader;

        [SerializeField]
        private float rotSpeed;
        // Start is called before the first frame update
        void Start()
        {
            //DoTaskAfterSomeWait();
            instance = this;
            Invoke(nameof(DoTaskAfterSomeWait), 1f);
        }

        //private void Update()
        //{
        //    if (Time.realtimeSinceStartup >= 15f && gameObject.activeInHierarchy)
        //    {
        //        AllOtherAccess.instance.showPopupWith_Ok_Action("Please check you internet connection", () =>
        //        {
        //            //UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
        //            //StopAllCoroutines();
        //            //UIManager.instance.hideScreenOnly(UIScreens_ENM.Splash);
        //            //UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

        //        });
        //    }
        //}             


        public void DoTaskAfterSomeWait()
        {
            if (Necessary_data.IsLoggedIn)
            {
                switch (Necessary_data.LoginType)
                {
                    case "guest":
                        Debug.Log("------->>> Last logged in with Guest");
                        StartCoroutine(PlateformLoginManager.instance.GetUserByEmail(Necessary_data.PlayerEmail));
                        break;

                    case "facebook":
                        Debug.Log("-------->>> Last logged in with Facebook" + (FB.IsLoggedIn));
                        if (Application.platform == RuntimePlatform.OSXEditor)
                        {
                            string playerFacebookId = Necessary_data.PlayerID;
                            string _pname = Necessary_data.PlayerName;
                            string _email = Necessary_data.PlayerEmail;
                            PlateformLoginManager.instance.GetUserLoginData(playerFacebookId, _pname, _email, "facebook");
                        }
                        else
                        {
                            Debug.Log("---->Facebook silent login CALL");
                            FacebookManager.instance.DealWithFbMenus();
                        }
                        break;

                    case "google":
                        Debug.Log("------->>> Last logged in with Google");

                        if (Application.platform == RuntimePlatform.OSXEditor)
                        {
                            string playerFacebookId = Necessary_data.PlayerID;
                            string _pname = Necessary_data.PlayerName;
                            string _email = Necessary_data.PlayerEmail;
                            PlateformLoginManager.instance.GetUserLoginData(playerFacebookId, _pname, _email, "google");        //from "facebook" to "google"
                        }
                        else
                        {
                            Debug.Log("---->Google silent login CALL");
                            GoogleSignInManager.instance.OnSignInSilently();
                        }

                        break;

                    default:
                        Debug.Log("------->>> Last logged Unknown");
                        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.loginscreen);
                        break;
                }

                //NetworkManager.instance.playerDetails.player_ID = Necessary_data.PlayerID;
                //NetworkManager.instance.playerDetails.player_Name = Necessary_data.PlayerName;
                //NetworkManager.instance.playerDetails.player_Email = Necessary_data.PlayerEmail;
                //NetworkManager.instance.playerDetails.player_LoginType = Necessary_data.LoginType;
                //NetworkManager.instance.playerDetails.player_Coin = Necessary_data.PlayerCoins;
                //NetworkManager.instance.playerDetails.player_historyDays = Necessary_data.playerHistoryDays;

                //UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
            }           ///------
            else if (Necessary_data.IsInternetAccessUnavailable())
            {
                //AllOtherAccess.instance.showPopupWith_Ok_Action("Oops! \nNo Internet Connection", null);
            }
            else
            {
                UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.loginscreen);
            }
        }


        // Update is called once per frame
        private void FixedUpdate()
        {
            loader.Rotate(rotSpeed * Time.deltaTime * Vector3.back);
        }
    }
}