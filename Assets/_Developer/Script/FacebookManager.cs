using System;
using System.Collections;
using System.Collections.Generic;

using Facebook.Unity;
using UnityEngine;

public class FacebookManager : MonoBehaviour
{
    public static FacebookManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    void Start()
    {
        FacebookInitialization();
    }

    private void FacebookInitialization()
    {
        print("======Facebook Initialization=====");

        if (!FB.IsInitialized)
        {
            print("---->FB not initialized<----");
            FB.Init(InitCallback);
        }
        else
        {
            print("---->FB initialized<----");
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        Debug.Log("======init call back=====");

        if (FB.IsInitialized)
        {
            print("----> IF FB.IsInitialized<----" + (FB.IsLoggedIn));
            FB.ActivateApp();

            if (FB.IsLoggedIn)
            {
                Debug.Log("if already logged in");
                DealWithFbMenus();
            }
        }
        else
        {
            print("Failed to Initialize the Facebook SDK");
        }
    }

    public void FBLogin()
    {
        if (Necessary_data.IsInternetAccessUnavailable())
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Oops! \nNo Internet Connection", null);
        }
        else
        {
            print("------True CONDITION------ : " + (FB.IsInitialized));
            //FB.Android.RetrieveLoginStatus(LoginStatusCallback);
            List<string> permissions = new List<string>();

            permissions.Add("public_profile");
            permissions.Add("gaming_profile");
            permissions.Add("gaming_user_picture");
            permissions.Add("user_friends");
            permissions.Add("email");

            FB.LogInWithReadPermissions(permissions, AuthCallBack);
        }
    }

    void AuthCallBack(IResult result)
    {
        print("-------->FB Loiginn res : " + result.RawResult);
        if (result.Error != null)
        {
            print(result.Error);
            AllOtherAccess.instance.showPopupWith_Ok_Action("Facebook data receiving error - 001", null);
            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                print("--->FB Login success!");

                DealWithFbMenus();

            }
            else
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Facebook data receiving error - 002", null);
                print("Facebook is not Logged in!");
                UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            }
        }
    }

    public void DealWithFbMenus()
    {

        //Call the FB Api to get the Player's Profile Picture
        FB.API("me/picture?height=500&redirect=false", HttpMethod.GET, DisplayProfilePic);
    }

    /// <summary>
    /// Callback function to get the Player's profile picture of his/her Facebook account
    /// And then get the Player's details of his/her Facebook account
    /// </summary>
    /// <param name="result"></param>
    void DisplayProfilePic(IGraphResult result)
    {
        Debug.Log("----111--------->Disp Prof Pic response " + result.ToString());
        Debug.Log("----222--------->Disp Prof Pic response " + result.RawResult);
        Debug.Log("----333--------->Disp Prof Pic response " + result.Error + "     Condition : " + (String.IsNullOrEmpty(result.Error)) + " ---- " + (!result.Cancelled));


        if (String.IsNullOrEmpty(result.Error) && !result.Cancelled)
        {
            IDictionary data = result.ResultDictionary["data"] as IDictionary; //create a new data dictionary
            string photoURL = data["url"] as String; //add a URL field to the dictionary

            //Call to FB API to get the Player's Facebook data
            FB.API("me?fields=id,email,name", HttpMethod.GET, GetFacebookData);
            _ = StartCoroutine(NetworkManager.instance.getAllQuiz());           //------      

            Necessary_data.profilePicURL = photoURL;

            Debug.Log("Hii there");
        }
        else
        {
            //AllOtherAccess.instance.showPopupWith_Ok_Action("Facebook data receiving error - 003", null);
            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
        }
    }

    void GetFacebookData(IGraphResult result)
    {
        Debug.Log("---->GetFacebookData----->" + result.RawResult);

        string playerFacebookId = result.ResultDictionary["id"].ToString();
        string _pname = result.ResultDictionary["name"].ToString(); //+ " " + result.ResultDictionary["last_name"].ToString();
        string _email = result.ResultDictionary.ContainsKey("email") ? result.ResultDictionary["email"].ToString() : "";
        PlateformLoginManager.instance.GetUserLoginData(playerFacebookId, _pname, _email, "facebook", result.RawResult);
    }

    public void CallLogout()
    {
        _ = StartCoroutine("FBLogout");
    }

    IEnumerator FBLogout()
    {
        FB.LogOut();
        while (FB.IsLoggedIn)
        {
            print("Logging Out");
            yield return null;
        }

        print("Logout Successful");
    }

}
