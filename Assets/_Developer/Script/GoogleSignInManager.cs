using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.UI;
using Google;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;
using System.Collections;
using SimpleJSON;

public class GoogleSignInManager : MonoBehaviour
{
    public static GoogleSignInManager instance;
    public string webClientId = "<your client id here>";

    //773004620282-2rvb6tg0iij7ql9avfv30qfdevlafrrl.apps.googleusercontent.com

    private GoogleSignInConfiguration configuration;

    private void Awake()
    {
        instance = this;

        configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
    }
    public void SignInWithGoogle()
    {
        OnSignIn();
        //OnSignInSilently();
    }
    public void SignOutFromGoogle() { OnSignOut(); }

    private void OnSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    public void OnSignOut()
    {
        GoogleSignIn.DefaultInstance.SignOut();
        print("Signed Out");
    }

    public void OnDisconnect()
    {
        GoogleSignIn.DefaultInstance.Disconnect();
        print("Disconnecting");
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {

        if (!task.IsFaulted)
        {

            Debug.Log("Welcome:AuthCode " + task.Result.AuthCode + "!");
            Debug.Log("Welcome:Name " + task.Result.DisplayName + "!");
            Debug.Log("Welcome:Email " + task.Result.Email + "!");
            Debug.Log("Welcome:Image " + task.Result.ImageUrl + "!");
            Debug.Log("Welcome:UserID " + task.Result.UserId + "!");

            string PlayerID = task.Result.UserId;
            string PlayerName = task.Result.DisplayName;
            string PlayerEmail = task.Result.Email;
            string LoginType = "google";
            string prof_pic_URL = task.Result.ImageUrl.ToString();

            Necessary_data.profilePicURL = prof_pic_URL;

            PlateformLoginManager.instance.GetUserLoginData(PlayerID, PlayerName, PlayerEmail, LoginType);
        }
        else if (task.IsCanceled)
        {
            Debug.Log("----------------------Canceled");
            AllOtherAccess.instance.showPopupWith_Ok_Action("Login Canceled", null);
        }
        else
        {
            Debug.Log("Task is faulted");
            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            //    using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            //    {
            //        if (enumerator.MoveNext())
            //        {
            //            AllOtherAccess.instance.showPopupWith_Ok_Action("Google data receiving error - 001", null);
            //            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            //            GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
            //            Debug.Log("1111 ------------- Got Faulted: " + error.Status + " " + error.Message);
            //        }
            //        else
            //        {
            //            Debug.Log("222 -------------- Got Unexpected Exception?!?" + task.Exception);
            //            AllOtherAccess.instance.showPopupWith_Ok_Action("Google data receiving error - 002", null);
            //            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            //        }
            //    }
        }

    }

    public void OnSignInSilently()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        print("Calling SignIn Silently");

        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnAuthenticationFinished);
    }

    public void OnGamesSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = true;
        GoogleSignIn.Configuration.RequestIdToken = false;

        print("Calling Games SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }


}