using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SignUpScreen : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField inp_unm, inp_email, inp_pass, inp_c_pass;

    [SerializeField]
    private Image passwordShoHide;

    [SerializeField]
    private Image cnfrm_passwordShoHide;

    [SerializeField]
    private TextMeshProUGUI txtUnmError, txtEmailError, txtPassError, txtconfrmPassError;


    [SerializeField]
    private int passFontSize;
    [SerializeField]
    private int textFontSize;

    JSONNode jsonData;
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnEnable()
    {
        _ResetData();
    }

    private void _ResetData()
    {
        hideAllToolTrip();

        inp_unm.text = "";
        inp_email.text = "";
        inp_pass.text = "";
        inp_c_pass.text = "";

        inp_pass.contentType = TMP_InputField.ContentType.Password;
        passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;
        inp_pass.pointSize = textFontSize;

        inp_c_pass.contentType = TMP_InputField.ContentType.Password;
        cnfrm_passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;
        inp_c_pass.pointSize = textFontSize;
    }


    public void hideAllToolTrip()
    {
        txtUnmError.transform.parent.parent.gameObject.SetActive(false);
        txtEmailError.transform.parent.parent.gameObject.SetActive(false);
        txtPassError.transform.parent.parent.gameObject.SetActive(false);
        txtconfrmPassError.transform.parent.parent.gameObject.SetActive(false);
    }

    public void OnInputFienFocue()
    {
        hideAllToolTrip();
    }


    private void showError(TextMeshProUGUI txt, string msg)
    {
        txt.transform.parent.parent.gameObject.SetActive(true);
        txt.text = msg;
    }

    public void OnClickSignUp()
    {
        if (!Necessary_data.IsInternetAccessUnavailable())
        {
            if (NetworkManager.instance.wantToEnableFastLogin)
            {
                inp_email.text = inp_unm.text + "@gmail.com";
                inp_pass.text = "111111aA@";

                Tuple<bool, string> passTupple = Necessary_data.isPasswordDoesNotMeetAllReq(inp_pass.text);

                //if (Necessary_data.isEmptyInputField(inp_unm.text))
                //{
                //    showError(txtUnmError, "Please enter username.");
                //}
                //else if (Necessary_data.isEmptyInputField(inp_email.text))
                //{
                //    showError(txtEmailError, "Please enter email id.");
                //}
                //else if (!Necessary_data.isValidEmail(inp_email.text))
                //{
                //    showError(txtEmailError, Necessary_data.info_InvalidEmail);
                //}
                //else if (Necessary_data.isEmptyInputField(inp_pass.text))
                //{
                //    showError(txtPassError, "Please enter password.");
                //}
                //else if (passTupple.Item1)
                //{
                //    showError(txtPassError, passTupple.Item2);
                //}
                //else if (drpdn_country.value == 0)
                //{
                //    showError(txtContryError, "Please select country.");
                //}
                //else
                {
                    UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
                    StartCoroutine(SendSignUpQuery());
                }
            }
            else
            {
                inp_unm.text = inp_unm.text.Trim();
                inp_pass.text = inp_pass.text.Trim();
                inp_c_pass.text = inp_c_pass.text.Trim();
                inp_email.text = inp_email.text.Trim();


                Tuple<bool, string> passTupple = Necessary_data.isPasswordDoesNotMeetAllReq(inp_pass.text);

                if (Necessary_data.isEmptyInputField(inp_unm.text))
                {
                    showError(txtUnmError, "Please enter username.");
                }
                else if (inp_unm.text.Length < 4)
                {
                    showError(txtUnmError, "Username must have more than 6 characters");
                }
                else if (Necessary_data.isEmptyInputField(inp_email.text))
                {
                    showError(txtEmailError, "Please enter email id.");
                }
                else if (!Necessary_data.isValidEmail(inp_email.text))
                {
                    showError(txtEmailError, Necessary_data.info_InvalidEmail);
                }
                else if (Necessary_data.isEmptyInputField(inp_pass.text))
                {
                    showError(txtPassError, "Please enter password.");
                }
                else if (passTupple.Item1)
                {
                    showError(txtPassError, passTupple.Item2);
                }
                else if (Necessary_data.isEmptyInputField(inp_c_pass.text))
                {
                    showError(txtPassError, "Please Re-enter password.");
                }
                else if (inp_c_pass.text != inp_pass.text)
                {
                    showError(txtPassError, "Password does not matched.");
                }
                else
                {
                    UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
                    StartCoroutine(SendSignUpQuery());
                }
            }
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Unable to login due to network issues", null);
        }
    }

    public void onClickFacebookLogin()
    {
        hideAllToolTrip();
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        FacebookManager.instance.FBLogin();
    }

    public void onClickGoogleLogin()
    {
        hideAllToolTrip();
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        //SignInSample.GoogleSignInScript.instance.GoogleLoginButtonClick();
        GoogleSignInManager.instance.SignInWithGoogle();
    }

    public void onClickAlreadyHaveAccout()
    {
        hideAllToolTrip();
        UIManager.instance.showNextScreenAndHideCurrentScreen(UIScreens_ENM.loginscreen, UIScreens_ENM.signupscreen);
    }

    public void onClickForgetPassword()
    {
        hideAllToolTrip();
    }


    public void onClickPasswordShowHide()
    {

        hideAllToolTrip();
        string temp_inp_pass = inp_pass.text;       //------
        Debug.Log("---------" + (passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon));

        if (passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon)
        {
            inp_pass.text = "";     //-----
            inp_pass.pointSize = textFontSize;
            inp_pass.contentType = TMP_InputField.ContentType.Standard;
            passwordShoHide.sprite = AllOtherAccess.instance.closedEyeIcon;
            inp_pass.text = temp_inp_pass;      //------
            //inp_pass.Select();        //openskeyboard
        }
        else
        {

            if (inp_pass.text == "")
            {
                inp_pass.pointSize = textFontSize;
            }
            else
            {
                inp_pass.text = "";
                inp_pass.pointSize = passFontSize;
            }
            inp_pass.contentType = TMP_InputField.ContentType.Password;
            passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;
            inp_pass.text = temp_inp_pass;      //------
            //inp_pass.Select();        //openskeyboard
        }
    }

    public void onClickConfirm_PasswordShowHide()
    {
        hideAllToolTrip();
        string temp_conf_inp_pass = inp_c_pass.text;        //----
        if (cnfrm_passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon)
        {
            inp_c_pass.text = "";           //------
            inp_c_pass.pointSize = textFontSize;
            inp_c_pass.contentType = TMP_InputField.ContentType.Standard;
            cnfrm_passwordShoHide.sprite = AllOtherAccess.instance.closedEyeIcon;
            inp_c_pass.text = temp_conf_inp_pass;      //------
            //inp_c_pass.Select();
        }
        else
        {
            if (inp_pass.text == "")
            {
                inp_c_pass.pointSize = textFontSize;
            }
            else
            {
                inp_c_pass.text = "";       //-----
                inp_c_pass.pointSize = passFontSize;
            }
            inp_c_pass.contentType = TMP_InputField.ContentType.Password;
            cnfrm_passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;
            inp_c_pass.text = temp_conf_inp_pass;      //------
            //inp_c_pass.Select();
        }
    }

    public void onEditInpuField(GameObject _obj)
    {
        TMP_InputField tMP_Input = _obj.GetComponent<TMP_InputField>();
        Image passwordShoHide = _obj.transform.parent.GetChild(0).GetComponent<Image>();

        if (passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon) // ***** 
        {
            if (tMP_Input.text == "")
            {
                tMP_Input.pointSize = textFontSize;
            }
            else
            {
                tMP_Input.pointSize = passFontSize;
            }
        }
        else //text 
        {
            tMP_Input.pointSize = textFontSize;
        }
    }

    private IEnumerator SendSignUpQuery()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);


        int rdomAvtarID = UnityEngine.Random.Range(0, 8);
        jsonData = new JSONObject
        {
            ["name"] = inp_unm.text,
            ["email"] = inp_email.text,
            ["password"] = inp_pass.text,
            ["source"] = "Email"
        };
        Debug.Log("----->>> Request For ------> Register " + NetworkManager.SignUp + "  Parameters : " + jsonData.ToString());      //from Networkmanager.login o signUp cuz' client has changed the "Register API to check duplicate user registration"

        UnityWebRequest result = UnityWebRequest.Put(NetworkManager.SignUp, jsonData.ToString());
        result.method = UnityWebRequest.kHttpVerbPOST;
        result.SetRequestHeader("Content-Type", "application/json");
        result.SetRequestHeader("Accept", "application/json");

        yield return result.SendWebRequest();


        JSONNode results = JSONNode.Parse(result.downloadHandler.text);

        Debug.Log("------>SignUp Response---- > : " + result.downloadHandler.text + "     ;  " + result.responseCode);

        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

        if (result.responseCode == 0)
        {
            Debug.Log("res code 000 ");
            AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (010)", null);
        }
        else
        {
            if (results.HasKey("Error"))
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action(results["Error"], null);
            }
            else if (results.HasKey("status") && results["status"] == 0)
            {
                Debug.Log("user already exitst STATUS = " + results["status"] + " MESSAGE = " + results["message"]);
                AllOtherAccess.instance.showPopupWith_Ok_Action(results["message"], () => UIManager.instance.showScreenOnly(UIScreens_ENM.signupscreen));

            }
            else
            {
                Debug.Log("setting user data on login success");
                PlateformLoginManager.instance.SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
            }

            //if (results["status"] == 1)//Signup success
            //{

            //    PlateformLoginManager.instance.SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
            //    AllOtherAccess.instance.showPopupWith_Ok_Action("Account created successfully.", null);

            //}
            //else if (results["status"] == 0)//No user exist //no valid email id
            //{
            //    showError(txtEmailError, Necessary_data.info_EmailNotFound);
            //}
            //else if (results["status"] == 2)// email already exists
            //{
            //    showError(txtEmailError, Necessary_data.info_EmailExist);
            //}
            //else if (results["status"] == 3)//user name already exists
            //{
            //    showError(txtUnmError, Necessary_data.info_UsernameExist);
            //}
            //else
            //{
            //    showError(txtEmailError, results["message"]);
            //}
        }
    }
}
