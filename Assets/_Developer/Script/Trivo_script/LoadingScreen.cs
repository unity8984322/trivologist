using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadingScreen : MonoBehaviour
{
    [SerializeField]
    private Transform loader;

    [SerializeField]
    private float rotSpeed;

    private void Update()
    {
        if (!UIManager.instance.uiScreens[0].screen.activeInHierarchy)
        {
            loader.transform.parent.gameObject.SetActive(true);
        }

        loader.Rotate(rotSpeed * Time.deltaTime * Vector3.back);
    }
}
