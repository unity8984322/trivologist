using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalanderScript : MonoBehaviour
{
    public static CalanderScript instance;

    public Sprite redBg, greenBG, defaultDayBg;

    [SerializeField]
    private Color c_dark;

    private void Awake()
    {
        if (instance != this)
            instance = this;
        else if (instance == null)
            Destroy(gameObject);
    }

    public void onClickCloseCalander()
    {
        UIManager.instance.hideScreenOnly(UIScreens_ENM.Calander);
    }


    public void onClickPrev_day()
    {


    }

    public void onClickNext_day()
    {


    }

    public void OnClickDate()
    {
    }




    //private static DateTime selectedDate = DateTime.Now;

    //class DateCallback : AndroidJavaProxy
    //{
    //    public DateCallback() : base("android.app.DatePickerDialog$OnDateSetListener") { }

    //    void onDateSet(AndroidJavaObject view, int year, int monthOfYear, int dayOfMonth)
    //    {
    //        selectedDate = new DateTime(year, monthOfYear + 1, dayOfMonth);
    //        Debug.Log("Picked Date is : " + selectedDate);
    //    }
    //}

    //void OnGUI()
    //{
    //    Debug.Log("On  gui  ");
    //    if (GUI.Button(new Rect(15, 225, 450, 75), "OKOOKO"))
    //    {

    //    }

    //    if (GUI.Button(new Rect(15, 15, 450, 75), string.Format("{0:yyyy-MM-dd}", selectedDate)))
    //    {
    //        Debug.Log("Button click for pick date");
    //        var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
    //        activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
    //        {
    //            new AndroidJavaObject("android.app.DatePickerDialog", activity, new DateCallback(), selectedDate.Year, selectedDate.Month - 1, selectedDate.Day).Call("show");
    //        }));
    //    }
    //}


}
