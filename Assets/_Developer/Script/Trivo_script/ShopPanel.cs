using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ShopPanel : MonoBehaviour
{
    public static ShopPanel instance;

    public TextMeshProUGUI txtCoin;
    public List<ShopPanelDetail> shopPanels;


    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        setShopData();
    }


    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoin;
    }



    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoin;
    }




    private void setCoin()
    {
        txtCoin.text = Necessary_data.PlayerCoins.ToString();
    }

    private void setShopData()
    {
        for (int i = 0; i < shopPanels.Count; i++)
        {
            shopPanels[i].txtPrice.text = "" + shopPanels[i].price;
            shopPanels[i].txtAmnout.text = "" + shopPanels[i].coinAmount + " Coins";
        }
    }

    public void OnClickShopPanelBuy(int panelIndex)
    {

        //switch (panelIndex)
        //{
        //    case 0:
        //        FindObjectOfType<Purchaser>().BuyConsumable();
        //        break;
        //    case 1:
        //        FindObjectOfType<Purchaser>().BuyConsumable1();
        //        break;
        //    case 2:
        //        FindObjectOfType<Purchaser>().BuyConsumable2();
        //        break;
        //    case 3:
        //        FindObjectOfType<Purchaser>().BuyConsumable3();
        //        break;

        //    default:
        //        break;
        //}


        InAppPurchaseManager.instance.BuyConsumableCoin(panelIndex);
        foreach (var item in shopPanels)
        {
            item.buyBtn.interactable = false;
        }
    }

    public void giveCoinOnBuySuccess(int id)
    {
        ShopPanelDetail shopPanel = shopPanels[id];
        StartCoroutine(NetworkManager.instance.buyCoins(Necessary_data.PlayerID, shopPanel.coinAmount, "  On purchase coin from shop"));

    }


    public void buyFailed()
    {
        foreach (var item in shopPanels)
        {
            Debug.Log("Buying coin failed");
            item.buyBtn.interactable = true;
        }
    }

    public void OnClickClose()
    {
        if (PowerUpManager.instance.quizAdventureScreen.activeInHierarchy)
        {
            Debug.Log("Closed shop panel when in quizadventure screen");
            QuizAdvantureScreen.instance.InvokeRepeating(nameof(QuizAdvantureScreen.instance.setQuestionTimer), 0, 1);      //----
        }
        UIManager.instance.hideScreenOnly(UIScreens_ENM.Shop);
    }
}


[System.Serializable]
public class ShopPanelDetail
{
    public GameObject panel;
    public TextMeshProUGUI txtAmnout;
    public TextMeshProUGUI txtPrice;
    public float price;
    public int coinAmount;
    public UnityEngine.UI.Button buyBtn;
}
