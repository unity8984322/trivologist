using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class QuizButton : MonoBehaviour
{
    public static Action<int> onSelectQuiz;
    public GameObject tickMarkImg, lockImg;
    public Image btnBG;
    public TextMeshProUGUI txtQuizNum;


    public void onClickQuizBtn(int quizNum)
    {
        List<int> attendedQusNumers = GameManager.instance.currentGameQuiz.attentdedQuestionNumber;

        if (attendedQusNumers.Count == 0 && quizNum == 0)
        {
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdvantureAScreen);
            onSelectQuiz?.Invoke(quizNum);
        }
        else if (attendedQusNumers.Count > 0 && (quizNum <= attendedQusNumers[attendedQusNumers.Count - 1] + 1))
        {
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdvantureAScreen);
            onSelectQuiz?.Invoke(quizNum);
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Attempt question sequentialy", null);
        }
    }
}
