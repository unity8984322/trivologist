using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public class QuesOfDayScreen : MonoBehaviour
{
    public static Action OnAnswerSendSuccess, OnAnswerSendFailed;
    public static Action<DateTime, bool, bool> OnQuestionOfGivenDateIsReceived;
    public static Action<DateTime, bool> OnPickDateFromCalander;

    [SerializeField]
    private Button confirmBtn;

    private Transform userSelectedOptionBtn;


    [SerializeField]
    private Image[] ansOptions;

    [SerializeField]
    private TextMeshProUGUI txtQuestion, txtDate, txtQusInfo;


    [SerializeField]
    private Sprite btnNormalBG, btnCurrectAnsBG, btnInCorrectBG, btnSelectedAnsBG;

    [SerializeField]
    private Color selectedAnsColor;

    [SerializeField]
    private DateTime currentDate;

    public string _currentDate;

    [SerializeField]
    private GameObject infoPopup;
    [SerializeField]
    private TextMeshProUGUI txtQuestionINFO;



    [Header("Powerup Details")]
    [SerializeField]
    private GameObject removeOption_PWRUP;

    [SerializeField]
    private GameObject freeAnswer_PWRUP;

    [SerializeField]
    private bool isTwoGuess_PWRUP_active;
    private int qAttemptCounterInTwoGuessPowerupMode;

    [SerializeField]
    private List<powerUPdata> powerUPdatas;

    private bool isAnswerSubmitted;


    private void OnEnable()
    {
        currentDate = DateTime.Today;

        OnQuestionOfGivenDateIsReceived += setQuestionDataToScreen_ForQuestionOfGivendate;
        OnPickDateFromCalander += onDatePickedFromCalander;
        OnAnswerSendSuccess += AnswerSuccess;
        OnAnswerSendFailed += AnswerFailed;

        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        StartCoroutine(NetworkManager.instance.getQuestionOfGivenDate(DateTime.Today, true));
        StartCoroutine(enableDisablePowerUpsPanel(true, 0.1f));

        //foreach (AllPowerUpsPanel item in PowerUpManager.instance.listOfAllPowerUpsPanel)
        //{
        //    for (int i = 0; i < 3; i++)
        //    {
        //        item.powerUpPanel.transform.GetChild(i).GetComponent<Button>().interactable = true;
        //    }
        //}

        //enableDisablePowerUpsPanel(true);           //------
        PowerUpManager.instance.listOfAllPowerUpsPanel[2].powerUpPanel.transform.GetChild(2).GetComponent<Button>().interactable = true;       //-----
    }

    private void OnDisable()
    {
        OnQuestionOfGivenDateIsReceived -= setQuestionDataToScreen_ForQuestionOfGivendate;
        OnPickDateFromCalander -= onDatePickedFromCalander;
        OnAnswerSendSuccess -= AnswerSuccess;
        OnAnswerSendFailed -= AnswerFailed;
    }

    IEnumerator enableDisablePowerUpsPanel(bool value, float time)
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < 3; i++)
        {
            PowerUpManager.instance.listOfAllPowerUpsPanel[2].powerUpPanel.transform.GetChild(i).GetComponent<Button>().interactable = value;
        }
    }

    public void subscribeEvent()
    {
        GameManager.OnUpdatePowerup += setPowerUp;
    }


    public void unSubscribeEvent()
    {
        GameManager.OnUpdatePowerup -= setPowerUp;
    }

    void Update()
    {
        _currentDate = Necessary_data.dateIn_MM_DD_YYYY(currentDate);
    }


    private void setQuestionDataToScreen_ForQuestionOfGivendate(DateTime dateThatPassdAtCallingTime, bool dataFetched, bool isTodaysDateQuistion)
    {
        isAnswerSubmitted = false;
        confirmBtn.interactable = false;
        txtQuestion.text = "";
        Canvas.ForceUpdateCanvases();

        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].gameObject.SetActive(true);
            ansOptions[i].GetComponent<Button>().interactable = true;
            ansOptions[i].sprite = btnNormalBG;
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
        }
        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

        if (dataFetched)
        {
            currentDate = dateThatPassdAtCallingTime;
            ansOptions = AllOtherAccess.instance.ShuffleArray(ansOptions);
            ansOptions[0].GetComponentInChildren<TextMeshProUGUI>().text = GameManager.instance.quizOfTheDay.q_RightAns;
            ansOptions[1].GetComponentInChildren<TextMeshProUGUI>().text = GameManager.instance.quizOfTheDay.q_WrongAns1;
            ansOptions[2].GetComponentInChildren<TextMeshProUGUI>().text = GameManager.instance.quizOfTheDay.q_WrongAns2;
            ansOptions[3].GetComponentInChildren<TextMeshProUGUI>().text = GameManager.instance.quizOfTheDay.q_WrongAns3;
            txtQuestion.text = GameManager.instance.quizOfTheDay.q_Question;
            txtDate.text = Necessary_data.dateIn_MM_DD_YYYY(dateThatPassdAtCallingTime);
            txtQusInfo.text = GameManager.instance.quizOfTheDay.q_MoreInfo;

            txtQusInfo.alignment = TextAlignmentOptions.Left;


            removeOption_PWRUP = ansOptions[1].gameObject;
            freeAnswer_PWRUP = ansOptions[0].gameObject;
        }
        else
        {
            if (isTodaysDateQuistion)
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found! Please try later!", () =>
                {
                    UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
                });
            }
            else
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Data not found! Please try later!", null);
            }
        }
    }

    public void onClickAns(GameObject selecteOption)
    {
        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            ansOptions[i].sprite = btnNormalBG;
        }
        userSelectedOptionBtn = selecteOption.transform;
        userSelectedOptionBtn.GetComponent<Image>().sprite = btnSelectedAnsBG;
        userSelectedOptionBtn.GetComponentInChildren<TextMeshProUGUI>().color = selectedAnsColor;

        confirmBtn.interactable = true;
    }

    public void OnClickConfirmAnsBtn()
    {
        StartCoroutine(confirmAnswer());
        StartCoroutine(enableDisablePowerUpsPanel(false, 0.1f));
        //enableDisablePowerUpsPanel(false);           //------
    }

    private IEnumerator confirmAnswer()
    {

        freeAnswer_PWRUP.transform.GetChild(0).gameObject.SetActive(false);
        freeAnswer_PWRUP.GetComponent<Animation>().Stop();

        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponent<Button>().interactable = false;
        }
        confirmBtn.interactable = false;


        QuestionDetail question = GameManager.instance.quizOfTheDay;
        string userAns = userSelectedOptionBtn.GetComponentInChildren<TextMeshProUGUI>().text;
        isAnswerSubmitted = true;

        if (Necessary_data.isAnswerMatched(userAns, question.q_RightAns))
        {
            //Necessary_data.addCoin(10);
            //StartCoroutine(NetworkManager.instance.buyCoins(Necessary_data.PlayerID, 10, "Add 10 coin on correct ans in quiz of day"));     //need to call this in submitplayeranswer
            userSelectedOptionBtn.GetComponent<Image>().sprite = btnCurrectAnsBG;

            isTwoGuess_PWRUP_active = false;

            _ = NetworkManager.instance.SendPlayerAnswerAsync(Necessary_data.PlayerID, GameManager.instance.quizOfTheDay.q_Id, "true", "1", false);       //-------
            //StartCoroutine(NetworkManager.instance.submitPlayerAnwser(Necessary_data.PlayerID, GameManager.instance.quizOfTheDay.q_Id, true, "1", false));
            yield return new WaitForSeconds(0.1f);

            confirmBtn.interactable = false;

        }
        else
        {
            userSelectedOptionBtn.GetComponent<Image>().sprite = btnInCorrectBG;
            if (!isTwoGuess_PWRUP_active || (isTwoGuess_PWRUP_active && qAttemptCounterInTwoGuessPowerupMode == 2))
            {
                yield return new WaitForSeconds(0.5f);
                ansOptions[0].sprite = btnCurrectAnsBG;

                ansOptions[0].transform.DOScale(Vector3.one * 1.15f, 0.3f)
                    .OnComplete(() => ansOptions[0].transform.DOScale(Vector3.one, 0.3f));
                _ = NetworkManager.instance.SendPlayerAnswerAsync(Necessary_data.PlayerID, GameManager.instance.quizOfTheDay.q_Id, "false", "1", false);       //-------
                //StartCoroutine(NetworkManager.instance.submitPlayerAnwser(Necessary_data.PlayerID, GameManager.instance.quizOfTheDay.q_Id, false, "1", false));
                yield return new WaitForSeconds(1.5f);

                confirmBtn.interactable = false;

                isTwoGuess_PWRUP_active = false;
            }
            else if (isTwoGuess_PWRUP_active && qAttemptCounterInTwoGuessPowerupMode == 1)
            {
                Invoke(nameof(re_EnableConfrirmBtnAfterFirstTryInTwoGuessPWRUP), 2);
            }
            qAttemptCounterInTwoGuessPowerupMode++;
        }
    }


    private void re_EnableConfrirmBtnAfterFirstTryInTwoGuessPWRUP()
    {
        confirmBtn.interactable = false;










        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponent<Button>().interactable = true;
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            ansOptions[i].sprite = btnNormalBG;
        }

    }

    private void AnswerSuccess()
    {
        //for (int i = 0; i < ansOptions.Length; i++)
        //{
        //    ansOptions[i].GetComponent<Button>().interactable = true;
        //}
    }

    private void AnswerFailed()
    {
        //for (int i = 0; i < ansOptions.Length; i++)
        //{
        //    ansOptions[i].GetComponent<Button>().interactable = true;
        //}
    }


    public void onClickBack()
    {
        isTwoGuess_PWRUP_active = false;        //-------
        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
        //Debug.Log("currentPowerUPIndex after going on home screen = " + PowerUpManager.instance.currentPowerUpIndex);
    }

    public void onClickNext()
    {
        //enableDisablePowerUpsPanel(true);           //------

        DateTime nextDate = Necessary_data.addDayInDate(1, currentDate);
        if (!Necessary_data.isGivenDateIsLaterThanToday(nextDate))
        {
            StartCoroutine(enableDisablePowerUpsPanel(true, 1f));
            UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            StartCoroutine(NetworkManager.instance.getQuestionOfGivenDate(nextDate, false));
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("No question available for next day.", null);
        }
    }

    public void onClickPrev()
    {
        //enableDisablePowerUpsPanel(true);           //------

        DateTime prevDate = Necessary_data.addDayInDate(-1, currentDate);

        if (Necessary_data.isDateValidForHistory(prevDate, Necessary_data.playerHistoryDays))
        {
            StartCoroutine(enableDisablePowerUpsPanel(true, 1f));
            UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            StartCoroutine(NetworkManager.instance.getQuestionOfGivenDate(prevDate, false));
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Your history days limit over.", null);
        }
    }

    public void onClickCalander()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.Calander);
    }

    private void onDatePickedFromCalander(DateTime date, bool isValidDate)
    {
        UIManager.instance.hideScreenOnly(UIScreens_ENM.Calander);
        if (isValidDate)
        {
            if (Necessary_data.isDateValidForHistory(date, Necessary_data.playerHistoryDays))
            {
                UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
                StartCoroutine(NetworkManager.instance.getQuestionOfGivenDate(date, false));
            }
            else
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Your history days limit over.", null);
            }
        }
    }



    public void OnClickShowQuestioInfo()
    {
        if (isAnswerSubmitted)
            infoPopup.SetActive(true);
    }

    public void closeInfoPopup()
    {
        infoPopup.SetActive(false);
    }

    public void OnClickUsePowerUp(int index)           //powerup    --inspector this is for the powerups in the question of the day screen
    {

        PowerUpManager.instance.currentPowerUpIndex = index;        //----- powerups in QuestionOftheDay screen working due to this line
        //PowerUpManager.instance.listOfAllPowerUpsPanel[2].powerUpPanel.transform.GetChild(index).GetComponent<Button>().interactable = false;           //----

        //(string) powerups --> DoubleGuess           FreeAnswer          RemoveAnswer
        switch (index)
        {
            case 0:

                //Two Guess PowerUp
                if (Necessary_data.PlayerPowerUp_0 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure you want to use this powerup?", () =>
                    {
                        isTwoGuess_PWRUP_active = true;
                        qAttemptCounterInTwoGuessPowerupMode = 1;
                        StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "DoubleGuess"));
                    }, null);
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have this powerup, Do you want to purchase it?",
                        () =>
                        {
                            PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                            UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                        },
                        () =>
                        {
                            //PowerUpManager.instance.listOfAllPowerUpsPanel[2].powerUpPanel.transform.GetChild(index).GetComponent<Button>().interactable = true;     //----
                        });
                }
                break;

            case 1:

                //Free Answer PowerUp
                if (Necessary_data.PlayerPowerUp_1 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure you want to use this powerup?", () =>
                    {
                        freeAnswer_PWRUP.GetComponent<Animation>().Play();
                        StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "FreeAnswer"));
                        onClickAns(ansOptions[0].gameObject);       //------
                        OnClickConfirmAnsBtn();         //------
                    }, null);
                }
                else
                {
                    //AllOtherAccess.instance.showPopupWith_Ok_Action("Please purchase powerup and try later", null);
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have this powerup, Do you want to purchase it?",
                       () =>
                       {
                           PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                           UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                           //StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "newRemoveAnswerCount"));
                       }, null);
                }

                break;

            case 2:

                //Remove two option PowerUp
                if (Necessary_data.PlayerPowerUp_2 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure you want to use this powerup?", () =>
                    {
                        removeOption_PWRUP.SetActive(false);
                        StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "RemoveAnswer"));
                        PowerUpManager.instance.listOfAllPowerUpsPanel[2].powerUpPanel.transform.GetChild(2).GetComponent<Button>().interactable = false;       //-----
                    }, null);
                }
                else
                {
                    //AllOtherAccess.instance.showPopupWith_Ok_Action("Please purchase powerup and try later", null);
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have powerup,Do you want to purchase powerup",
                       () =>
                       {
                           PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                           UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                           //    powerupPanel.SetActive(false);
                       }, null);
                    ;
                }

                break;

            default:
                break;
        }
    }

    private void setPowerUp()
    {
        for (int i = 0; i < powerUPdatas.Count; i++)
        {
            if (i == 0)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_0 + "";
            else if (i == 1)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_1 + "";
            else if (i == 2)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_2 + "";
        }
    }
}
