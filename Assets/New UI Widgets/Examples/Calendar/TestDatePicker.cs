﻿namespace UIWidgets.Examples
{
    using System;
    using System.Globalization;
    using UIWidgets;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Test DatePicker.
    /// </summary>
    public class TestDatePicker : MonoBehaviour, IUpgradeable
    {
        /// <summary>
        /// DatePicker template.
        /// </summary>
        [SerializeField]
        protected DatePicker PickerTemplate;

        /// <summary>
        /// Text component to display selected value.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        [Obsolete("Replaced with InfoAdapter.")]
        protected Text Info;

        /// <summary>
        /// Text component to display selected value.
        /// </summary>
        [SerializeField]
        protected TextAdapter InfoAdapter;

        DateTime currentValue = DateTime.Today;

        /// <summary>
        /// Open picker and log selected value.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "HAA0603:Delegate allocation from a method group", Justification = "Required")]
        public void Test()
        {
            // create picker from template
            var picker = PickerTemplate.Clone();

            // show picker
            picker.Show(currentValue, ValueSelected, Canceled);
        }

        void ValueSelected(DateTime value)
        {
            currentValue = value;
            Debug.Log(string.Format("value: {0}", value.ToString()));
        }

        void Canceled()
        {
            Debug.Log("canceled");
            currentValue = DateTime.Today;
        }

        /// <summary>
        /// Open picker and display selected value.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "HAA0603:Delegate allocation from a method group", Justification = "Required")]
        public void TestShow()
        {
            // create picker from template
            var picker = PickerTemplate.Clone();
            currentValue = DateTime.Today;

            // show picker
            picker.Show(currentValue, ShowValueSelected, ShowCanceled);
        }

        void ShowValueSelected(DateTime value)
        {
            currentValue = value;
            if (!Necessary_data.isGivenDateIsLaterThanToday(value))
            {
                string x = value.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture);

                InfoAdapter.text = x;

                QuesOfDayScreen.OnPickDateFromCalander?.Invoke(value, true);

                Debug.Log("------>Selected Date is : " + value + "   -----    " + x);//+ string.Format("Value: {0}", value.ToString()));
            }
            else
                QuesOfDayScreen.OnPickDateFromCalander?.Invoke(value, false);

        }

        void ShowCanceled()
        {
            InfoAdapter.text = "Canceled";
            currentValue = DateTime.Today;
        }

        /// <summary>
        /// Upgrade this instance.
        /// </summary>
        public virtual void Upgrade()
        {
#pragma warning disable 0612, 0618
            Utilities.GetOrAddComponent(Info, ref InfoAdapter);
#pragma warning restore 0612, 0618
        }

#if UNITY_EDITOR
        /// <summary>
        /// Validate this instance.
        /// </summary>
        protected virtual void OnValidate()
        {
            Compatibility.Upgrade(this);
        }
#endif
    }
}