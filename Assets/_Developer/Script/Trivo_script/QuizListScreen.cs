using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuizListScreen : MonoBehaviour
{
    public static QuizListScreen instance;

    public static Action OnUnlockNextQuiz;

    [SerializeField]
    private TextMeshProUGUI txtCoins;

    [SerializeField]
    private SelectQuizButton QuizBtnPreafab;

    [SerializeField]
    private Transform content;

    [SerializeField]
    private Sprite green, yellow, blue;

    [SerializeField]
    private Color c_green, c_yellow, c_blue;

    public List<SelectQuizButton> quizButtons;

    public void Awake()
    {
        instance = this;
        setData();
    }


    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoins;
        //if (ResultScreen.isQuizComplete)                //--------
        OnUnlockNextQuiz += unLockQuiz;
    }

    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoins;
        OnUnlockNextQuiz -= unLockQuiz;
    }


    private void OnEnable()
    {
        for (int i = 0; i < quizButtons.Count; i++)         //i=1
        {
            if (i < NetworkManager.instance.playerDetails.completeQuiz)  // if (PlayerPrefs.GetInt("quiz_" + i.ToString()) == 1)
            {
                quizButtons[i].isLocked = false;
            }
            else
            {
                quizButtons[i].isLocked = true;
            }

            updateButtons(quizButtons[i]);
        }
    }

    public void setData()
    {
        AllOtherAccess.instance.ClearChildren(content);

        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

        Canvas.ForceUpdateCanvases();

        for (int i = 0; i < GameManager.instance.allQuizes.Count; i++)
        {
            spawnButtons(i);
        }
    }


    private void spawnButtons(int i)
    {
        SelectQuizButton quizButton = Instantiate(QuizBtnPreafab, content);

        quizButton.quizNimber = "" + (1 + i);
        quizButton.txtQuizNum.text = GameManager.instance.allQuizes[i].Quiz_Tite;// "Quiz " + quizButton.quizNimber;


        //        Debug.Log("----Check quiz lock or unlock : --iii--  " + i + "   ---  " + NetworkManager.instance.playerDetails.completeQuiz);

        if (i < NetworkManager.instance.playerDetails.completeQuiz)  // if (PlayerPrefs.GetInt("quiz_" + i.ToString()) == 1)
        {
            quizButton.isLocked = false;
        }
        else
        {
            quizButton.isLocked = true;
        }

        quizButton.setQuizData(GameManager.instance.allQuizes[i]);
        quizButtons.Add(quizButton);


        updateButtons(quizButton);
    }

    private void updateButtons(SelectQuizButton quizButton)
    {

        if (!quizButton.isLocked)
        {
            quizButton.txtQuizNum.color = c_yellow;
            quizButton.btnBG.sprite = yellow;
            quizButton.tickMarkImg.SetActive(false);
            quizButton.lockImg.SetActive(false);
        }
        else if (quizButton.isLocked)
        {
            quizButton.txtQuizNum.color = c_blue;
            quizButton.btnBG.sprite = blue;
            quizButton.tickMarkImg.SetActive(false);
            quizButton.lockImg.SetActive(true);
        }
    }

    public void unLockQuiz()
    {
        Debug.Log("-----Action called for unlocking quiz manualy");

        for (int i = 0; i < quizButtons.Count; i++)
        {
            if (i < NetworkManager.instance.playerDetails.completeQuiz)
            {
                quizButtons[i].isLocked = false;
                Debug.Log("Unlocked Quiz Button = " + quizButtons[i].quizNimber);
            }
            else
            {
                quizButtons[i].isLocked = true;
                Debug.Log("Locked Quiz Button = " + quizButtons[i].quizNimber);
            }
            updateButtons(quizButtons[i]);
        }
    }

    private void setCoins()
    {
        txtCoins.text = Necessary_data.PlayerCoins.ToString();
    }

    public void onClickBack()
    {
        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
    }
}
