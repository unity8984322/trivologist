﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingScreen : MonoBehaviour
{
    public Transform music, sound;
    private GameObject S_on, S_off, M_on, M_off;
    static bool canAddListener;

    private void Awake()
    {
        canAddListener = true;
    }

    private void OnEnable()
    {
        S_on = sound.GetChild(1).gameObject;
        S_off = sound.GetChild(2).gameObject;


        M_on = music.GetChild(1).gameObject;
        M_off = music.GetChild(2).gameObject;

        if (canAddListener)
        {
            S_on.GetComponent<Button>().onClick.AddListener(() => { OnClickSound(); });
            S_off.GetComponent<Button>().onClick.AddListener(() => { OnClickSound(); });

            M_on.GetComponent<Button>().onClick.AddListener(() => { OnClickMusic(); });
            M_off.GetComponent<Button>().onClick.AddListener(() => { OnClickMusic(); });
            canAddListener = false;
        }
        setButtons();
    }

    private void setButtons()
    {
        if ((Necessary_data.isSoundOn))
        {
            SoundManager.instance.soundAudioSource.volume = 1;
            S_on.SetActive(true);
            S_off.SetActive(false);
        }
        else
        {

            SoundManager.instance.soundAudioSource.volume = 0;
            S_on.SetActive(false);
            S_off.SetActive(true);
        }

        if (Necessary_data.isMusicOn)
        {

            SoundManager.instance.BG_audioSource.volume = 1;
            M_on.SetActive(true);
            M_off.SetActive(false);
        }
        else
        {
            SoundManager.instance.BG_audioSource.volume = 0;
            M_on.SetActive(false);
            M_off.SetActive(true);
        }
    }

    public void OnClickSound()
    {
        if (Necessary_data.isSoundOn)
        {
            Necessary_data.isSoundOn = false;
        }
        else
        {
            Necessary_data.isSoundOn = true;
        }
        setButtons();
        SoundManager.instance.playBtnClickSound();
    }

    public void OnClickMusic()
    {
        if (Necessary_data.isMusicOn)
        {
            Necessary_data.isMusicOn = false;
        }
        else
        {
            Necessary_data.isMusicOn = true;
        }
        setButtons();
        SoundManager.instance.playBGSound();
    }

    public void onClickClose(bool wantToDestroyOnClose)
    {
        //M_on.GetComponent<Button>().onClick.RemoveAllListeners();
        //M_off.GetComponent<Button>().onClick.RemoveAllListeners();
        //S_on.GetComponent<Button>().onClick.RemoveAllListeners();
        //M_off.GetComponent<Button>().onClick.RemoveAllListeners();
        gameObject.SetActive(false);
    }
}
