using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AllOtherAccess : MonoBehaviour
{
    public static AllOtherAccess instance;

    public GameObject popupWith_Ok_Action, popupWith_Ok_Cancle_Action;

    public Sprite[] avtarSprites;

    public Sprite openEyeIcon, closedEyeIcon;

    public UIScreens_ENM lastScreen;

    static bool canInvokeAgain;

    [SerializeField]
    GameObject loadingScreen;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
        canInvokeAgain = true;
    }

    //private void Start()
    //{

    //    if (canInvokeAgain)
    //        InvokeRepeating(nameof(CheckInternetconnection), 0, 1);
    //}

    [Obsolete]
    private void Update()
    {
        if (canInvokeAgain)
            CheckInternetconnection();
    }


    [Obsolete]
    public void CheckInternetconnection()
    {

        Debug.Log("CheckINternetConnection called");

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {

            Debug.Log("NO Internet");
            canInvokeAgain = false;
            showPopupWith_Ok_Action("Check your internet connection",
                () =>
                {
                    if (Application.internetReachability != NetworkReachability.NotReachable) //internet is available
                    {
                        if (Necessary_data.IsLoggedIn && (GameManager.instance.homeScreen.isActiveAndEnabled || loadingScreen.activeInHierarchy))
                            StartCoroutine(PlateformLoginManager.instance.GetUserByEmail(Necessary_data.PlayerEmail));
                        else if (!Necessary_data.IsLoggedIn)
                            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.loginscreen);
                    }
                    canInvokeAgain = true;
                });
        }
        else canInvokeAgain = true;
    }

    public void logOutPlayer()
    {
        if (Necessary_data.LoginType == "google")
        {
            GoogleSignInManager.instance.OnSignOut();
        }
        else if (Necessary_data.LoginType == "facebook")
        {
            FacebookManager.instance.CallLogout();
        }

        Necessary_data.profilePicURL = "";
        Necessary_data.ProfPicBaseString = "";
        Necessary_data.PlayerID = "";
        Necessary_data.PlayerName = "";
        Necessary_data.PlayerEmail = "";
        Necessary_data.PlayerAvtarID = "";
        Necessary_data.LoginType = "";
        Necessary_data.IsLoggedIn = false;
        NetworkManager.instance.playerDetails = new PlayerDetails();
        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.loginscreen);
        PlayerPrefs.DeleteAll();
        GameManager.instance.calanderQuizdata.calanderAPIdatelist.Clear();          //------
    }

    public void onClickSend_helpnSupportMail()
    {
        string email = Necessary_data.supportEmailid;
        string subject = MyEscapeURL("DrawRelay");
        string body = MyEscapeURL("Write here");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }

    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void showPopupWith_Ok_Cancle_Action(string msg, Action yesAction, Action noAction)
    {
        Debug.Log("-------------showPopupWith_Ok_Cancle_Action------------------" + msg);

        GameObject popuppas = GameObject.Find("popup");

        if (popuppas)
        {

            Destroy(popuppas);
        }
        GameObject obj = Instantiate(popupWith_Ok_Cancle_Action, popupParent());
        obj.name = "popup";
        obj.GetComponent<PopupWithOk_Cancle_Action>().initData(msg, yesAction, noAction);



    }

    public void showPopupWith_Ok_Action(string msg, Action yesAction)
    {
        Debug.Log("-------------showPopupWith_Ok_Action------------------" + msg);
        if (GameObject.Find("popup"))
        {
            Destroy(GameObject.Find("popup"));
        }
        GameObject obj = Instantiate(popupWith_Ok_Action, popupParent());
        obj.name = "popup";
        obj.GetComponent<PopupWith_Ok_Action_Only>().initData(msg, yesAction);

    }

    private Transform popupParent()
    {

        return UIManager.instance.transform;

    }


    public Texture getAndSetProfPic(string avtarId, string baseKey)
    {
        if (avtarId == "null")
        {
            //SET ICON FROM BASE64
            return Necessary_data.GetTextureFromImageString(baseKey);
        }
        else
        {
            //SET AVTAR ICON
            try
            {
                return avtarSprites[int.Parse(avtarId)].texture;

            }
            catch (System.Exception ex)
            {
                return null;
            }
        }

    }


    public void ClearChildren(Transform parentt, bool isAnyRistriction = false)
    {
        int i = 0;

        //Array to hold all child obj
        GameObject[] allChildren = new GameObject[parentt.childCount];

        //Find all child obj and store to that array
        foreach (Transform child in parentt)
        {
            allChildren[i] = child.gameObject;
            i += 1;
        }

        //Now destroy them
        foreach (GameObject child in allChildren)
        {
            if (isAnyRistriction)
            {
                if (child.gameObject.activeSelf)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
            else
            {
                DestroyImmediate(child.gameObject);
            }
        }
    }

    public Image[] ShuffleArray(Image[] array)
    {
        System.Random _random = new System.Random();
        int p = array.Length;

        for (int n = p - 1; n > 0; n--)
        {
            int r = _random.Next(0, n);
            Image t = array[r];
            array[r] = array[n];
            array[n] = t;
        }
        return array;
    }
}
