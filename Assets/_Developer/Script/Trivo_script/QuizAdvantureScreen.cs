using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class QuizAdvantureScreen : MonoBehaviour
{
    public static QuizAdvantureScreen instance;

    public static Action OnAnswerSendSuccess, OnAnswerSendFailed;

    [SerializeField]
    private Image[] ansOptions;

    [SerializeField]
    private Color selectedAnsColor;

    [SerializeField]
    private TextMeshProUGUI txtTitle, txtQuestion, txtQuestionInfo, txtCoins, txtTimer;

    public TextMeshProUGUI txtQuizNumber;

    [SerializeField]
    private Sprite btnNormalBG, btnCurrectAnsBG, btnInCorrectBG, btnSelectedAnsBG;

    [SerializeField]
    private int currentQustionNUm;

    [SerializeField]
    private int questionNumberAtSeventyPreQuizFinish;

    [SerializeField]
    public int scoreOfCurrentQuiz = 0;

    [SerializeField]
    public int seventyFivePerScore = 0;

    [SerializeField]
    private Button confirmBtn, nextBnt;

    private Transform userSelectedOptionBtn;

    [SerializeField]
    private GameObject infoPopup;

    [Header("Powerup Details")]
    [SerializeField]
    private GameObject removeOption_PWRUP;

    [SerializeField]
    private GameObject freeAnswer_PWRUP;

    [SerializeField]
    private bool isTwoGuess_PWRUP_active;
    private int qAttemptCounterInTwoGuessPowerupMode;

    [SerializeField]
    private List<powerUPdata> powerUPdatas;

    private int usedTimeInSec;

    [SerializeField]
    private Animation animation;
    [SerializeField]
    private AnimationClip swipLeft, swipNext;

    public bool isNextQuizUnlocked;

    public void Awake()
    {
        instance = this;
    }

    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoins;
        GameManager.OnUpdatePowerup += setPowerUp;
    }


    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoins;
        GameManager.OnUpdatePowerup -= setPowerUp;
    }

    private void OnEnable()
    {
        PlayerPrefs.SetInt("unlockedQuiz", 0);
        isNextQuizUnlocked = false;
        //    QuizButton.onSelectQuiz += setQuestionAndDetails;
        OnAnswerSendSuccess += AnswerSuccess;
        OnAnswerSendFailed += AnswerFailed;



        removeOption_PWRUP = null;
        freeAnswer_PWRUP = null;
        qAttemptCounterInTwoGuessPowerupMode = 1;
        infoPopup.SetActive(false);
        setQuestionAndDetails(0);
        scoreOfCurrentQuiz = 0;
        txtQuizNumber.text = GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber - 1].Quiz_Tite;

        seventyFivePerScore = Mathf.FloorToInt((GameManager.instance.currentGameQuiz.quizAdvanture.Count * Necessary_data.coinValueOnCorrectAns) * 75 / 100f);

        StartCoroutine(enableDisablePowerupsPanel(true, 0.1f));
        //enableDisablePowerupsPanel(true);
    }


    public IEnumerator enableDisablePowerupsPanel(bool value, float time)
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < 3; i++)
        {
            PowerUpManager.instance.listOfAllPowerUpsPanel[1].powerUpPanel.transform.GetChild(i).GetComponent<Button>().interactable = value;
        }
    }

    private void OnDisable()
    {
        //   QuizButton.onSelectQuiz -= setQuestionAndDetails;
        OnAnswerSendSuccess -= AnswerSuccess;
        OnAnswerSendFailed -= AnswerFailed;
    }

    public void setQuestionTimer()
    {
        var ts = TimeSpan.FromSeconds(usedTimeInSec);
        txtTimer.text = string.Format("{00:00}:{01:00}", (int)ts.TotalMinutes, ts.Seconds);
        //Debug.Log("Time spent = " + ts.TotalMinutes + "-----" + ts.TotalSeconds);

        usedTimeInSec++;
    }

    public void ClearQ_AnswerLists()
    {
        GameManager.instance.currentGameQuiz.totalCorrectAnswer.Clear();        //--------- 
        GameManager.instance.currentGameQuiz.totalInCorrectAnswer.Clear();        //--------- 
        GameManager.instance.currentGameQuiz.attentdedQuestionNumber.Clear();        //---------
        GameManager.instance.currentGameQuiz.quizAdvanture.Clear();             //-------
        GameManager.instance.currentSelectedQuizNumber = 0;             //-------
    }

    private void setQuestionAndDetails(int quizNum)
    {
        animation.Play(swipNext.name);

        usedTimeInSec = 0;

        CancelInvoke(nameof(setQuestionTimer));
        InvokeRepeating(nameof(setQuestionTimer), 0, 1);

        txtTitle.text = "Questions-" + (1 + quizNum);
        confirmBtn.interactable = false;
        confirmBtn.gameObject.SetActive(true);
        nextBnt.gameObject.SetActive(false);

        currentQustionNUm = quizNum;
        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].gameObject.SetActive(true);
            ansOptions[i].sprite = btnNormalBG;
            ansOptions[i].GetComponent<Button>().interactable = true;
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
        }

        txtQuestion.text = "";
        txtQuestionInfo.text = "";
        Canvas.ForceUpdateCanvases();

        QuestionDetail questionData = GameManager.instance.currentGameQuiz.quizAdvanture[quizNum];

        txtQuestionInfo.text = questionData.q_MoreInfo;
        txtQuestionInfo.Rebuild(CanvasUpdate.Layout);
        txtQuestion.text = questionData.q_Question;

        ansOptions = AllOtherAccess.instance.ShuffleArray(ansOptions);

        ansOptions[0].GetComponentInChildren<TextMeshProUGUI>().text = questionData.q_RightAns;
        ansOptions[1].GetComponentInChildren<TextMeshProUGUI>().text = questionData.q_WrongAns1;
        ansOptions[2].GetComponentInChildren<TextMeshProUGUI>().text = questionData.q_WrongAns2;
        ansOptions[3].GetComponentInChildren<TextMeshProUGUI>().text = questionData.q_WrongAns3;

        removeOption_PWRUP = ansOptions[1].gameObject;
        freeAnswer_PWRUP = ansOptions[0].gameObject;

        //Canvas.ForceUpdateCanvases();
    }

    public void onClickAns(GameObject selecteOption)
    {
        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            ansOptions[i].sprite = btnNormalBG;
        }
        userSelectedOptionBtn = selecteOption.transform;
        userSelectedOptionBtn.GetComponent<Image>().sprite = btnSelectedAnsBG;
        userSelectedOptionBtn.GetComponentInChildren<TextMeshProUGUI>().color = selectedAnsColor;

        confirmBtn.interactable = true;
    }

    public void OnClickConfirmAnsBtn()
    {
        StartCoroutine(confirmAnswer());
        StartCoroutine(enableDisablePowerupsPanel(false, 0.1f));
        //enableDisablePowerupsPanel(false);          //-----
    }

    private IEnumerator confirmAnswer()
    {

        freeAnswer_PWRUP.transform.GetChild(0).gameObject.SetActive(false);
        freeAnswer_PWRUP.GetComponent<Animation>().Stop();

        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponent<Button>().interactable = false;
        }
        confirmBtn.interactable = false;
        nextBnt.interactable = true;

        QuestionDetail question = GameManager.instance.currentGameQuiz.quizAdvanture[currentQustionNUm];
        string userAns = userSelectedOptionBtn.GetComponentInChildren<TextMeshProUGUI>().text;

        if (currentQustionNUm == GameManager.instance.currentGameQuiz.totalQuestion - 1)
            nextBnt.GetComponentInChildren<TextMeshProUGUI>().text = "Submit";
        else
            nextBnt.GetComponentInChildren<TextMeshProUGUI>().text = "Next";

        CancelInvoke(nameof(setQuestionTimer));
        if (Necessary_data.isAnswerMatched(userAns, question.q_RightAns))
        {
            if (!GameManager.instance.currentGameQuiz.attentdedQuestionNumber.Contains(currentQustionNUm))
                GameManager.instance.currentGameQuiz.attentdedQuestionNumber.Add(currentQustionNUm);

            if (!GameManager.instance.currentGameQuiz.totalCorrectAnswer.Contains(currentQustionNUm))
                GameManager.instance.currentGameQuiz.totalCorrectAnswer.Add(currentQustionNUm);

            //StartCoroutine(NetworkManager.instance.buyCoins(Necessary_data.PlayerID, 10, "Add 10 coin on correct ans in quiz advanture"));      //Need to call this method in the submit player answer
            scoreOfCurrentQuiz += Necessary_data.coinValueOnCorrectAns;
            //if (scoreOfCurrentQuiz > seventyFivePerScore && PlayerPrefs.GetInt("unlockedQuiz") == 0)            //----------
            //{
            //    PlayerPrefs.SetInt("unlockedQuiz", 1);
            //    Debug.Log("-----------Next quiz unloked here----When player get score above 75%------Curr quiz Num : " + GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber].Quiz_Tite + "     unlocking quiz num is : " + GameManager.instance.allQuizes[GameManager.instance.currentSelectedQuizNumber + 1].Quiz_Tite);

            //    StartCoroutine(NetworkManager.instance.unloackNextQuiz(0, GameManager.instance.currentSelectedQuizNumber));
            //}

            userSelectedOptionBtn.GetComponent<Image>().sprite = btnCurrectAnsBG;

            isTwoGuess_PWRUP_active = false;

            //StartCoroutine(NetworkManager.instance.submitPlayerAnwser(Necessary_data.PlayerID, question.q_Id, true, usedTimeInSec.ToString(), true));           // from inside next btn to here


            yield return new WaitForSeconds(1);

            confirmBtn.gameObject.SetActive(false);
            //nextBnt.interactable = true;
            nextBnt.gameObject.SetActive(true);

            nextBnt.onClick.RemoveAllListeners();
            nextBnt.onClick.AddListener(() =>
            {

                _ = NetworkManager.instance.SendPlayerAnswerAsync(Necessary_data.PlayerID, question.q_Id, "true", usedTimeInSec.ToString(), true);        //------
                StartCoroutine(enableDisablePowerupsPanel(true, 1f));
                nextBnt.interactable = false;
                //enableDisablePowerupsPanel(true);       //------
            });
        }
        else
        {
            userSelectedOptionBtn.GetComponent<Image>().sprite = btnInCorrectBG;

            if (!isTwoGuess_PWRUP_active || (isTwoGuess_PWRUP_active && qAttemptCounterInTwoGuessPowerupMode == 2))
            {
                if (!GameManager.instance.currentGameQuiz.attentdedQuestionNumber.Contains(currentQustionNUm))
                    GameManager.instance.currentGameQuiz.attentdedQuestionNumber.Add(currentQustionNUm);

                if (!GameManager.instance.currentGameQuiz.totalInCorrectAnswer.Contains(currentQustionNUm))
                    GameManager.instance.currentGameQuiz.totalInCorrectAnswer.Add(currentQustionNUm);

                //infoPopup.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                //  infoPopup.SetActive(false);
                ansOptions[0].sprite = btnCurrectAnsBG;

                ansOptions[0].transform.DOScale(Vector3.one * 1.15f, 0.3f)
                    .OnComplete(() => ansOptions[0].transform.DOScale(Vector3.one, 0.3f));

                //StartCoroutine(NetworkManager.instance.submitPlayerAnwser(Necessary_data.PlayerID, question.q_Id, false, usedTimeInSec.ToString(), true));      //from next btn block to here
                yield return new WaitForSeconds(1.5f);

                confirmBtn.gameObject.SetActive(false);
                nextBnt.gameObject.SetActive(true);

                nextBnt.onClick.RemoveAllListeners();
                nextBnt.onClick.AddListener(() =>
                {

                    _ = NetworkManager.instance.SendPlayerAnswerAsync(Necessary_data.PlayerID, question.q_Id, "false", usedTimeInSec.ToString(), true);        //------
                    StartCoroutine(enableDisablePowerupsPanel(true, 1f));
                    nextBnt.interactable = false;
                });

                isTwoGuess_PWRUP_active = false;
            }
            else if (isTwoGuess_PWRUP_active && qAttemptCounterInTwoGuessPowerupMode == 1)
            {
                Invoke(nameof(re_EnableConfrirmBtnAfterFirstTryInTwoGuessPWRUP), 2);
            }
            qAttemptCounterInTwoGuessPowerupMode++;
        }

    }

    private void AnswerSuccess()
    {
        StartCoroutine(playAnimForNextQues());
    }

    IEnumerator playAnimForNextQues()
    {
        currentQustionNUm++;

        if (currentQustionNUm < GameManager.instance.currentGameQuiz.quizAdvanture.Count)
        {
            //QuizButton.onSelectQuiz?.Invoke(currentQustionNUm);
            animation.Play(swipLeft.name);
            yield return new WaitForSeconds(0.6f);
            setQuestionAndDetails(currentQustionNUm);
        }
        else
        {
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Result);
        }


    }

    private void AnswerFailed()
    {
        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponent<Button>().interactable = true;
        }
    }


    private void re_EnableConfrirmBtnAfterFirstTryInTwoGuessPWRUP()
    {
        confirmBtn.interactable = false;

        confirmBtn.gameObject.SetActive(true);
        nextBnt.gameObject.SetActive(false);


        for (int i = 0; i < ansOptions.Length; i++)
        {
            ansOptions[i].GetComponent<Button>().interactable = true;
            ansOptions[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            ansOptions[i].sprite = btnNormalBG;
        }
    }

    public void OnClickShowQuestioInfo()
    {
        if (nextBnt.gameObject.activeInHierarchy)
        {
            infoPopup.SetActive(true);
            StartCoroutine(tset());
        }
    }

    IEnumerator tset()
    {
        yield return new WaitForSeconds(2);
        //txtQuestionInfo.enabled = false;

        //yield return new WaitForSeconds(2);
        //txtQuestionInfo.enabled = true;

        //yield return new WaitForSeconds(2);
        //string aa = txtQuestionInfo.text;
        //aa += " _ ";
        //txtQuestionInfo.text = aa;
    }


    public void closeInfoPopup()
    {
        infoPopup.SetActive(false);
    }


    public void OnClickUsePowerUp(int index)        //powerup --inspector       this one is for the powerups within the quiz adventure screen
    {

        //CancelInvoke(nameof(setQuestionTimer));     //-----
        PowerUpManager.instance.currentPowerUpIndex = index;        //----- powerups in quizadventure screen working due to this line
        switch (index)
        {
            case 0:

                //Two Guess PowerUp
                if (Necessary_data.PlayerPowerUp_0 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure you want to use this powerup?",
                     () =>
                     {
                         isTwoGuess_PWRUP_active = true;
                         qAttemptCounterInTwoGuessPowerupMode = 1;
                         StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "DoubleGuess"));

                     }, null);
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have this powerup, Do you want to purchase it",
                        () =>
                        {
                            CancelInvoke(nameof(setQuestionTimer));
                            PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                            UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                            //     powerupPanel.SetActive(false);
                            //StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "newRemoveAnswerCount"));            //--------
                        },
                        () =>
                        {

                            Invoke(nameof(setQuestionTimer), 0);
                        });
                }

                break;

            case 1:

                //Free Answer PowerUp
                if (Necessary_data.PlayerPowerUp_1 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure want to use this powerup?", () =>
                    {
                        freeAnswer_PWRUP.GetComponent<Animation>().Play();
                        StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "FreeAnswer"));
                        onClickAns(ansOptions[0].gameObject);       //-------
                        OnClickConfirmAnsBtn();             //-------

                    }, null);
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have powerup, Do you want to purchase powerup",
                       () =>
                       {
                           CancelInvoke(nameof(setQuestionTimer));
                           PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                           UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                           //StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "newRemoveAnswerCount"));
                       },
                       () =>
                       {

                           Invoke(nameof(setQuestionTimer), 0);
                       });
                }
                break;

            case 2:

                //Remove two option PowerUp
                if (Necessary_data.PlayerPowerUp_2 > 0)
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure you want to use this powerup?", () =>
                    {
                        removeOption_PWRUP.SetActive(false);
                        StartCoroutine(NetworkManager.instance.usePowerUp(Necessary_data.PlayerID, "RemoveAnswer"));
                        PowerUpManager.instance.listOfAllPowerUpsPanel[1].powerUpPanel.transform.GetChild(2).GetComponent<Button>().interactable = false;       //-----
                    }, null);
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have powerup, Do you want to purchase powerup",
                       () =>
                       {
                           CancelInvoke(nameof(setQuestionTimer));
                           PowerUpManager.instance.whenClickPurchasePowerBtn();            //------
                           UIManager.instance.showScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
                       },
                       () =>
                       {

                           Invoke(nameof(setQuestionTimer), 0);
                       });
                }



                break;

            default:
                break;
        }
    }

    public void onClickShop()
    {
        CancelInvoke(nameof(setQuestionTimer));
        UIManager.instance.showScreenOnly(UIScreens_ENM.Shop);
    }

    public void onClickBack()
    {

        AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("Are you sure ?\nYou will lose all of your quiz progress!!",
            () =>
            {
                isTwoGuess_PWRUP_active = false;        //------
                CancelInvoke(nameof(setQuestionTimer));
                UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizListScreen);
                ClearQ_AnswerLists();
            },
             null);

        //UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdventure_selection);
        // GameManager.OnQdavantureQuizReceived?.Invoke();
    }

    public void setCoins()
    {
        txtCoins.text = Necessary_data.PlayerCoins.ToString();
    }

    private void setPowerUp()               //powerup
    {
        Debug.Log(Necessary_data.PlayerPowerUp_0 + "     --      " + Necessary_data.PlayerPowerUp_1 + "      --    " + Necessary_data.PlayerPowerUp_2);

        for (int i = 0; i < powerUPdatas.Count; i++)
        {
            if (i == 0)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_0 + "";
            else if (i == 1)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_1 + "";
            else if (i == 2)
                powerUPdatas[i].txtPowerUPCount.text = Necessary_data.PlayerPowerUp_2 + "";
        }
    }
}

[Serializable]
class powerUPdata
{
    public TextMeshProUGUI txtPowerUPCount;
    public Button useBtn;
}