﻿namespace UIWidgets
{
    using System;
    using UIWidgets.Styles;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    /// <summary>
    /// Base class for Calendar date.
    /// </summary>
    public class CalendarDateBase : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        /// <summary>
        /// Text component to display day.
        /// </summary>
        [SerializeField]
        protected TextAdapter dayAdapter;

        /// <summary>
        /// Text component to display day.
        /// </summary>
        public TextAdapter DayAdapter
        {
            get
            {
                return dayAdapter;
            }

            set
            {
                dayAdapter = value;
                DateChanged();
            }
        }

        /// <summary>
        /// Image component to display day background.
        /// </summary>
        [SerializeField]
        public Image DayImage;

        /// <summary>
        /// Selected date background.
        /// </summary>
        [SerializeField]
        public Sprite SelectedDayBackground;

        /// <summary>
        /// Selected date color.
        /// </summary>
        [SerializeField]
        public Color SelectedDay = Color.white;

        /// <summary>
        /// Default date background.
        /// </summary>
        [SerializeField]
        public Sprite DefaultDayBackground;

        /// <summary>
        /// Color for date in current month.
        /// </summary>
        [SerializeField]
        public Color CurrentMonth = Color.white;

        /// <summary>
        /// Weekend date color.
        /// </summary>
        [SerializeField]
        public Color Weekend = Color.red;

        /// <summary>
        /// Color for date not in current month.
        /// </summary>
        [SerializeField]
        public Color OtherMonth = Color.gray;

        /// <summary>
        /// Current date to display.
        /// </summary>
        protected DateTime CurrentDate;

        /// <summary>
        /// Date belongs to this calendar.
        /// </summary>
        [HideInInspector]
        public CalendarBase Calendar;

        /// <summary>
        /// Set current date.
        /// </summary>
        /// <param name="currentDate">Current date.</param>
        public virtual void SetDate(DateTime currentDate)
        {
            CurrentDate = currentDate;

            DateChanged();
        }

        /// <summary>
        /// Update displayed date.
        /// </summary>
        public virtual void DateChanged()
        {
            DayAdapter.text = CurrentDate.ToString("dd", Calendar.Culture);

            if (Calendar.IsSameDay(Calendar.Date, CurrentDate))
            {
                DayAdapter.color = SelectedDay;
                DayImage.sprite = SelectedDayBackground;
            }
            else
            {

                //Debug.Log("---calender date : " + Calendar.Date + "     current date : " + CurrentDate);

                //Debug.Log("<color=violet>CurrentDate = </color>" + CurrentDate);
                calanderAPIdata getCurrentDateFromAPIDates = GameManager.instance.calanderQuizdata.calanderAPIdatelist.Find(x => x._date == Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));

                if (getCurrentDateFromAPIDates != null)
                {
                    Debug.Log("<color=yellow>==========></color> -------  Date available in API List  - " + Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));

                    if (getCurrentDateFromAPIDates._date == Necessary_data.dateIn_MM_DD_YYYY(CurrentDate))
                    {
                        //This block will check the Calander Dates with the API Calander dates which client has given whether the answer is given or not
                        //--Currently only  3 Dates are saved in client's API

                        Debug.Log("<color=green>==========></color> -------  this is same day  (Calendar date and API Date matched)- " + Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));
                        if (getCurrentDateFromAPIDates.isCorrectAns/* && getCurrentDateFromAPIDates.date.Month == Days[i].Calendar.dateDisplay.Month*/) //------
                        {
                            //Debug.Log("Days[i].Calendar.dateDisplay.Month = " +/* Days[i].Calendar.dateDisplay.Month + */" && " + "getCurrentDateFromAPIDates.date.Month = " + getCurrentDateFromAPIDates.date.Month);

                            DayImage.sprite = CalanderScript.instance.greenBG;
                            Debug.Log("user gives correct ans");

                        }
                        else if (!getCurrentDateFromAPIDates.isCorrectAns)
                        {
                            DayImage.sprite = CalanderScript.instance.redBg;
                            Debug.Log("------user gives incorrect answer  - " + Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));
                        }
                    }
                    else
                    {
                        Debug.Log("<color=red>==========></color> -------  this is  not same day  - " + Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));
                    }
                }
                else
                {
                    //Debug.Log("<color=blue>==========></color> -------  Date not available not in API List  - " + Necessary_data.dateIn_MM_DD_YYYY(CurrentDate));
                    DayImage.sprite = CalanderScript.instance.defaultDayBg;
                    //Debug.Log(DayImage.sprite.name);
                }


                //  DayImage.sprite = DefaultDayBackground;     //XYZ       This line is setting the background color for the date

                if (Calendar.IsSameMonth(Calendar.DateDisplay, CurrentDate))
                {
                    //if (Calendar.IsWeekend(CurrentDate) ||
                    //	Calendar.IsHoliday(CurrentDate))
                    //{
                    //	DayAdapter.color = Weekend;
                    //}
                    //else
                    {
                        DayAdapter.color = CurrentMonth;
                    }
                }
                else
                {
                    //if (Calendar.IsWeekend(CurrentDate) ||
                    //	Calendar.IsHoliday(CurrentDate))
                    //{
                    //	DayAdapter.color = Weekend * OtherMonth;
                    //}
                    //else
                    {
                        DayAdapter.color = OtherMonth;
                        dayAdapter.GetComponent<Text>().raycastTarget = false;
                        dayAdapter.transform.parent.GetComponent<Image>().raycastTarget = false;
                    }
                }

                if (CurrentDate < Calendar.DateMin)
                {
                    DayAdapter.color *= OtherMonth;
                }
                else if (CurrentDate > Calendar.DateMax)
                {
                    DayAdapter.color *= OtherMonth;
                }
            }
        }

        /// <summary>
        /// OnPoiterDown event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnPointerDown(PointerEventData eventData)
        {
        }

        /// <summary>
        /// OnPointerUp event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnPointerUp(PointerEventData eventData)
        {
        }

        /// <summary>
        /// PointerClick event.
        /// Change calendar date to clicked date.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (!Calendar.IsActive())
            {
                return;
            }

            if (CurrentDate < Calendar.DateMin)
            {
                return;
            }

            if (CurrentDate > Calendar.DateMax)
            {
                return;
            }

            Calendar.Date = CurrentDate;
            Calendar.OnDateClick.Invoke(CurrentDate);
        }

        /// <summary>
        /// Apply specified style.
        /// </summary>
        /// <param name="styleCalendar">Style for the calendar.</param>
        /// <param name="style">Full style data.</param>
        public virtual void SetStyle(StyleCalendar styleCalendar, Style style)
        {
            if (DayAdapter != null)
            {
                styleCalendar.DayText.ApplyTo(DayAdapter.gameObject);
            }

            //styleCalendar.DayBackground.ApplyTo(DayImage);        //XYZ

            DefaultDayBackground = styleCalendar.DayBackground.Sprite;        //XYZ
            SelectedDayBackground = styleCalendar.SelectedDayBackground;   //XYZ

            SelectedDay = styleCalendar.ColorSelectedDay;
            Weekend = styleCalendar.ColorWeekend;

            CurrentMonth = styleCalendar.ColorCurrentMonth;
            OtherMonth = styleCalendar.ColorOtherMonth;

            if (Calendar != null)
            {
                DateChanged();
            }
        }

        /// <summary>
        /// Set style options from widget properties.
        /// </summary>
        /// <param name="styleCalendar">Style for the calendar.</param>
        /// <param name="style">Full style data.</param>
        public virtual void GetStyle(StyleCalendar styleCalendar, Style style)
        {
            if (DayAdapter != null)
            {
                styleCalendar.DayText.GetFrom(DayAdapter.gameObject);
            }

            styleCalendar.DayBackground.GetFrom(DayImage);

            styleCalendar.DayBackground.Sprite = DefaultDayBackground;      //XYZ
            styleCalendar.SelectedDayBackground = SelectedDayBackground;//XYZ

            styleCalendar.ColorSelectedDay = SelectedDay;
            styleCalendar.ColorWeekend = Weekend;

            styleCalendar.ColorCurrentMonth = CurrentMonth;
            styleCalendar.ColorOtherMonth = OtherMonth;
        }
    }
}