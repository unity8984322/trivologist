using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using DG.Tweening;

public class PopupWithOk_Cancle_Action : MonoBehaviour
{

    [SerializeField]
    private TextMeshProUGUI txtMsg;

    [SerializeField]
    private Button btnYes, btnNo;



    public void initData(string msg, Action yesAction, Action noAction)
    {
        showPopup();
        txtMsg.text = msg;
        btnYes.onClick.AddListener(() =>
        {
            yesAction?.Invoke();
            destroyPopup();
        });

        btnNo.onClick.AddListener(() =>
        {
            noAction?.Invoke();
            destroyPopup();
        });
    }

    private void showPopup()
    {
        RectTransform popupRect = transform.GetChild(0).GetComponent<RectTransform>();
        popupRect.anchoredPosition = new Vector2(0, 1700);
        popupRect.DOAnchorPosY(-100, 0.3f)
            .SetEase(Ease.Linear)
            .OnComplete(() => popupRect.DOAnchorPosY(0, 0.1f)
                .SetEase(Ease.Linear));
    }

    private void destroyPopup()
    {
        RectTransform popupRect = transform.GetChild(0).GetComponent<RectTransform>();
        popupRect.DOAnchorPosY(-100, 0.1f)
            .SetEase(Ease.Linear)
            .OnComplete(() => popupRect.DOAnchorPosY(1700, 0.3f)
                .SetEase(Ease.Linear)
                .OnComplete(() => DestroyImmediate(gameObject)));
    }

}
