using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections;
using Unity.Services.Core;
using Unity.Services.Core.Environments;

public class InAppPurchaseManager : MonoBehaviour, IStoreListener
{
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
    public static InAppPurchaseManager instance;

    public Product product_c1;
    public Product product_c2;
    public Product product_c3;
    public Product product_c4;

    public GameObject shop, outerObject, InternetDialog, loader;


    public int[] P1Coins;

    public static string prod_1 = "com.diamond100";
    public static string prod_2 = "com.diamond200";
    public static string prod_3 = "com.diamond500";
    public static string prod_4 = "com.diamond1000";

    //		T_Money tMoney;
    public string environment = "production";       //-------

    private void Awake()
    {
        instance = this;
    }

    async void Start()
    {
        try                             //-----------
        {
            var options = new InitializationOptions()
                .SetEnvironmentName(environment);

            await UnityServices.InitializeAsync(options);
        }
        catch (Exception exception)
        {
            Debug.Log("Error catched = " + exception);
        }
        //----------




        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }

        //			tMoney = GameObject.FindObjectOfType<T_Money> ();
    }


    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            Debug.Log("Already Initialized");
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        builder.AddProduct(prod_1, ProductType.Consumable);
        builder.AddProduct(prod_2, ProductType.Consumable);
        builder.AddProduct(prod_3, ProductType.Consumable);
        builder.AddProduct(prod_4, ProductType.Consumable);

        UnityPurchasing.Initialize(this, builder);
        Debug.Log("It wasn't intialized but not should be intialized --> m_StoreController = " + m_StoreController == null + " m_StoreExtensionProbider = " + m_StoreExtensionProvider.Equals(null));
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        //Debug.Log("If initialized then --> m_StoreController = " + m_StoreController == null + " m_StoreExtensionProbider = " + m_StoreExtensionProvider.Equals(null));
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    public void BuyConsumableCoin(int id)
    {
        //loader.SetActive(true);
        switch (id)
        {

            case 0:
                StartCoroutine(checkpurchase(product_c1));
                break;

            case 1:
                StartCoroutine(checkpurchase(product_c2));
                break;

            case 2:
                StartCoroutine(checkpurchase(product_c3));
                break;

            case 3:
                StartCoroutine(checkpurchase(product_c4));
                break;

            default:
                break;
        }
    }

    public IEnumerator checkpurchase(Product pro)
    {
        Debug.Log("a");
        WWW www = new WWW("http://www.google.com");// http://google.com      
        yield return www;
        if (www.error != null)
        {
            Debug.Log("b");
            //outerObject.SetActive (true);
            //shop.SetActive(false);
            //loader.SetActive(false);
        }
        else
        {
            Debug.Log("c :: " + IsInitialized());
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }
            else
            {
                Debug.Log("already init");
            }
            m_StoreController.InitiatePurchase(pro);
            //loader.SetActive(false);
        }
    }



    void BuyProductID()
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // system's products collection.
            product_c1 = m_StoreController.products.WithID(prod_1);
            product_c2 = m_StoreController.products.WithID(prod_2);
            product_c3 = m_StoreController.products.WithID(prod_3);
            product_c4 = m_StoreController.products.WithID(prod_4);
        }
        // Otherwise ...
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        Debug.Log("Initialization Done");
        BuyProductID();
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        if (GameManager.instance.shopPanel.gameObject.activeInHierarchy)
            AllOtherAccess.instance.showPopupWith_Ok_Action("Products not available", null);            //--------
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error.ToString());        //----- .ToString()
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {

        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, prod_1, StringComparison.Ordinal))
        {

            Debug.Log(string.Format("ProcessPurchase: PASS Coin. Product: '{0}'", args.purchasedProduct.definition.id));
            //PlayerPrefs.SetInt ("TotalCoins", PlayerPrefs.GetInt ("TotalCoins") + P1Coins[0]);
            ShopPanel.instance.giveCoinOnBuySuccess(0);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, prod_2, StringComparison.Ordinal))
        {

            Debug.Log(string.Format("ProcessPurchase: PASS Diamond. Product: '{0}'", args.purchasedProduct.definition.id));
            //PlayerPrefs.SetInt ("TotalCoins", PlayerPrefs.GetInt ("TotalCoins") + P1Coins[1]);
            ShopPanel.instance.giveCoinOnBuySuccess(1);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, prod_3, StringComparison.Ordinal))
        {

            Debug.Log(string.Format("ProcessPurchase: PASS Diamond. Product: '{0}'", args.purchasedProduct.definition.id));
            //PlayerPrefs.SetInt ("TotalCoins", PlayerPrefs.GetInt ("TotalCoins") + P1Coins[1]);
            ShopPanel.instance.giveCoinOnBuySuccess(2);
        }
        else if (String.Equals(args.purchasedProduct.definition.id, prod_4, StringComparison.Ordinal))
        {

            Debug.Log(string.Format("ProcessPurchase: PASS Diamond. Product: '{0}'", args.purchasedProduct.definition.id));
            //PlayerPrefs.SetInt ("TotalCoins", PlayerPrefs.GetInt ("TotalCoins") + P1Coins[1]);
            ShopPanel.instance.giveCoinOnBuySuccess(3);
        }
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        Debug.Log(string.Format("Purchase faild on the device", product.definition.storeSpecificId, failureReason));
        ShopPanel.instance.buyFailed();
    }

	public void OnInitializeFailed(InitializationFailureReason error, string message)
	{
		throw new NotImplementedException();
	}















	////////////////////////
	///
	//public static InAppPurchaseManager instance;
	//private static IStoreController m_StoreController;          // The Unity Purchasing system.
	//private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.


	//public static string kProductIDConsumable1 = "com.item1";
	//public static string kProductIDConsumable2 = "com.item2";
	//public static string kProductIDConsumable3 = "com.item3";
	//public static string kProductIDConsumable4 = "com.item4";

	//private void Awake()
	//{
	//    if (instance != null) return;

	//    instance = this;

	//}

	//private void OnEnable()
	//{

	//    //if (m_StoreController != null)
	//    //    GetProductsPrice();
	//}
	//void Start()
	//{
	//    DontDestroyOnLoad(gameObject);

	//    // If we haven't set up the Unity Purchasing reference
	//    if (m_StoreController == null)
	//    {
	//        // Begin to configure our connection to Purchasing
	//        InitializePurchasing();
	//    }
	//    else
	//    {
	//        Debug.Log("**************************//**************************");
	//        GetProductsPrice();
	//    }

	//    //			tMoney = GameObject.FindObjectOfType<T_Money> ();
	//}

	//public void InitializePurchasing()
	//{
	//    Debug.Log("------=======-- -IsInitialized:  " + IsInitialized());

	//    // If we have already connected to Purchasing ...
	//    if (IsInitialized())
	//    {
	//        // ... we are done here.
	//        return;
	//    }

	//    // Create a builder, first passing in a suite of Unity provided stores.
	//    var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

	//    // Add a product to sell / restore by way of its identifier, associating the general identifier
	//    // with its store-specific identifiers.

	//    builder.AddProduct(kProductIDConsumable1, ProductType.Consumable);
	//    builder.AddProduct(kProductIDConsumable2, ProductType.Consumable);
	//    builder.AddProduct(kProductIDConsumable3, ProductType.Consumable);
	//    builder.AddProduct(kProductIDConsumable4, ProductType.Consumable);

	//    UnityPurchasing.Initialize(this, builder);
	//}


	//private bool IsInitialized()
	//{

	//    bool state = m_StoreController != null && m_StoreExtensionProvider != null;

	//    Debug.Log("-----InApp Purchase Init Status : " + state);
	//    // Only say we are initialized if both the Purchasing references are set.
	//    return m_StoreController != null && m_StoreExtensionProvider != null;
	//}



	//public void BuyConsumable(int id)
	//{
	//    string itemID = "";
	//    switch (id)
	//    {

	//        case 0:
	//            itemID = kProductIDConsumable1;
	//            break;
	//        case 1:
	//            itemID = kProductIDConsumable2;
	//            break;
	//        case 2:
	//            itemID = kProductIDConsumable3;
	//            break;
	//        case 3:
	//            itemID = kProductIDConsumable4;
	//            break;
	//        default:
	//            Debug.Log("Unknown ID ;");
	//            break;
	//    }
	//    Debug.Log("****  ID :  " + id + "  :  **********************//**************************22 :Prod ID :  " + itemID);
	//    BuyProductID(itemID);
	//}

	//void BuyProductID(string productId)
	//{
	//    // If Purchasing has been initialized ...
	//    if (IsInitialized())
	//    {
	//        Product product = m_StoreController.products.WithID(productId);
	//        // If the look up found a product for this device's store and that product is ready to be sold ... 
	//        if (product != null && product.availableToPurchase)
	//        {
	//            Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id) + "  " + product.transactionID);

	//            // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
	//            // asynchronously.
	//            m_StoreController.InitiatePurchase(product);

	//        }
	//        // Otherwise ...
	//        else
	//        {
	//            // ... report the product look-up failure situation  
	//            Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
	//        }
	//    }
	//    // Otherwise ...
	//    else
	//    {
	//        // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
	//        // retrying initiailization.
	//        Debug.Log("BuyProductID FAIL. Not initialized.");
	//    }

	//}

	///// <summary>
	///// Method to get the Price of each items as per price set on Console
	///// </summary>
	//public void GetProductsPrice()
	//{
	//    //shopItemsList[i].GetComponent<ShopItem>().itemprice.text = shopDataJSONResponse["data"][i]["price"].AsInt.ToString();
	//    //PlayerPrefs.SetString("price1", product1.metadata.localizedPriceString);

	//    //Debug.Log("Purchaser---->GetProductsPrice");

	//    //KBC.ShopManager.Instance.shopItemsList[i].GetComponent<KBC.ShopItem>().itemprice.text = productsList[i].metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[0].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem1").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[1].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem2").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[2].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem3").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[3].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem4").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[4].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem5").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[5].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem6").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[6].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem7").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[7].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem8").metadata.localizedPriceString;
	//    //ShopManager.Instance.shopItemsList[8].GetComponent<ShopItem>().itemprice.text = m_StoreController.products.WithID("com.kbcitem9").metadata.localizedPriceString;

	//}


	//public void RestorePurchases()
	//{
	//    // If Purchasing has not yet been set up ...
	//    if (!IsInitialized())
	//    {
	//        // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
	//        Debug.Log("RestorePurchases FAIL. Not initialized.");
	//        return;
	//    }

	//    // If we are running on an Apple device ... 
	//    if (Application.platform == RuntimePlatform.IPhonePlayer ||
	//        Application.platform == RuntimePlatform.OSXPlayer)
	//    {
	//        // ... begin restoring purchases
	//        Debug.Log("RestorePurchases started ...");

	//        // Fetch the Apple store-specific subsystem.
	//        var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
	//        apple.RestoreTransactions((result) =>
	//        {
	//            Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
	//        });
	//    }
	//    // Otherwise ...
	//    else
	//    {
	//        // We are not running on an Apple device. No work is necessary to restore purchases.
	//        Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
	//    }
	//}

	//public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	//{
	//    // Purchasing has succeeded initializing. Collect our Purchasing references.
	//    //			Debug.Log("OnInitialized: PASS");

	//    // Overall Purchasing system, configured with products for this application.
	//    m_StoreController = controller;
	//    // Store specific subsystem, for accessing device-specific store features.
	//    m_StoreExtensionProvider = extensions;
	//    GetProductsPrice();
	//}


	//public void OnInitializeFailed(InitializationFailureReason error)
	//{
	//    // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
	//    Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	//}


	//public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	//{
	//    // A consumable product has been purchased by this user.
	//    if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable1, StringComparison.Ordinal))//Add if successful
	//    {
	//        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + "  " + args.purchasedProduct.transactionID);
	//        Debug.Log("1st product purchased");
	//        FindObjectOfType<ShopPanel>().giveCoinOnBuySuccess(0);
	//    }
	//    else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
	//    {
	//        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
	//        Debug.Log("2st product purchased");
	//        FindObjectOfType<ShopPanel>().giveCoinOnBuySuccess(1);
	//    }
	//    else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
	//    {
	//        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
	//        Debug.Log("3st product purchased");
	//        FindObjectOfType<ShopPanel>().giveCoinOnBuySuccess(2);
	//    }
	//    else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable4, StringComparison.Ordinal))
	//    {
	//        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
	//        Debug.Log("4st product purchased");
	//        FindObjectOfType<ShopPanel>().giveCoinOnBuySuccess(3);
	//    }
	//    return PurchaseProcessingResult.Complete;
	//}


	//public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	//{
	//    // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
	//    // this reason with the user to guide their troubleshooting actions.
	//    Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	//}


}