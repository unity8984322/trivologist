using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public List<UiScree_Cls> uiScreens;

    private void Awake()
    {
        instance = this;
        transform.GetChild(0).gameObject.SetActive(false);
    }

    private void Start()
    {
        showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Splash);        //--------
    }

    //private void Update()
    //{
    //    //if (Input.GetKeyDown(KeyCode.Escape))
    //    //{
    //    //    if (uiScreens.Find(X => X.UiScreens_ENM == UIScreens_ENM.mainScrren).screen.activeInHierarchy)
    //    //    {
    //    //        Application.Quit();
    //    //    }
    //    //}
    //}


    public void showNextScreenAndHideAllOtherScreen(UIScreens_ENM screenToShow)
    {
        foreach (var item in uiScreens)
        {
            item.screen.SetActive(false);
        }

        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        scrn.screen.SetActive(true);
    }


    public void showNextScreenAndHideCurrentScreen(UIScreens_ENM screenToShow, UIScreens_ENM screenToHide)
    {
        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToHide);
        scrn.screen.SetActive(false);

        scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        scrn.screen.SetActive(true);
    }

    public void showScreenOnly(UIScreens_ENM screenToShow)
    {
        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToShow);
        scrn.screen.SetActive(true);
    }

    public void hideScreenOnly(UIScreens_ENM screenToHide)
    {
        UiScree_Cls scrn = uiScreens.Find(X => X.UiScreens_ENM == screenToHide);
        scrn.screen.SetActive(false);
    }
}
