using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Networking;
using SimpleJSON;
using DG.Tweening;

public class HomeScreen : MonoBehaviour
{
    public static HomeScreen instance;
    public RawImage profPic_Raw;

    [SerializeField]
    private TextMeshProUGUI txtPlayerName;

    [SerializeField]
    private TextMeshProUGUI txtCoins;

    [SerializeField]
    //private TextMeshProUGUI[] txtPowerUPCount;      //powerup


    public void Awake()
    {
        instance = this;
    }
    public void OnEnable()
    {
        setPLayerName();
        //PowerUpManager.instance.powerUpsBottomPanel.SetActive(true);
        PowerUpManager.instance.listOfAllPowerUpsPanel[0].powerUpPanel.SetActive(true);            //------
        //purchasePowerUPScreen.SetActive(false);
        //setProfPic();
    }

    private void OnDisable()
    {
        //PowerUpManager.instance.powerUpsBottomPanel.SetActive(false);
        PowerUpManager.instance.listOfAllPowerUpsPanel[0].powerUpPanel.SetActive(false);            //------
    }

    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoins;
        GameManager.OnProfPicSuccessfullyFetched += setProfilePicture;
        //GameManager.OnUpdatePowerup += setPowerUp;          //powerup
    }



    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoins;
        GameManager.OnProfPicSuccessfullyFetched -= setProfilePicture;
        //GameManager.OnUpdatePowerup += setPowerUp;      //powerup
    }


    public void onClickProfilePic()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.PlayerProfile);
    }

    public void onClickShop()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.Shop);
    }

    //public void onClickPowerUp(int id)              //powerup --inspector
    //{
    //    currentPowerUpIndex = id;
    //    whenClickPurchasePowerBtn();
    //}

    public void onClickAdvanture()
    {
        if (Necessary_data.IsInternetAccessUnavailable())
        {
            AllOtherAccess.instance.
                showPopupWith_Ok_Action("Please check you internet connection", () =>
            {
                UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
            });
        }
        else
        {

            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizListScreen);

            //===============================================================

            //GameManager.instance.resetGameData();

            //if (GameManager.instance.currentGameQuiz.quizAdvanture.Count == 0)
            //{
            //    UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            //    StartCoroutine(NetworkManager.instance.getAdvantureQuestions(GameManager.instance.currentGameQuiz.CurrentQuizID));
            //}
            //else
            //{
            //    UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuizAdventure_selection);
            //    GameManager.OnQdavantureQuizReceived?.Invoke();
            //}
        }
    }

    public void onClickQustionOfDay()
    {
        if (Necessary_data.IsInternetAccessUnavailable())
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Please check you internet connection", () =>
            {
                UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
            });
        }
        else
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.QuesOfDay);
    }

    public void onClickHowToPlay()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.HowToPlay);
    }

    public void onClickSettingBnt()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.settingScreen);
    }

    public void setProfilePicture(Texture2D texture2)
    {
        //Debug.Log("------Prof Pic texture set in Home Screen");

        profPic_Raw.texture = texture2;
    }



    private void setPLayerName()
    {
        txtPlayerName.text = Necessary_data.PlayerName;
    }

    private void setCoins()
    {
        txtCoins.text = Necessary_data.PlayerCoins.ToString();
    }

    //private void setPowerUp()               //powerup
    //{
    //    txtPowerUPCount[0].text = Necessary_data.PlayerPowerUp_0 + "";
    //    txtPowerUPCount[2].text = Necessary_data.PlayerPowerUp_1 + "";
    //    txtPowerUPCount[1].text = Necessary_data.PlayerPowerUp_2 + "";
    //}


    //#region POWER UP PURCHASE PANEL         
    //[Space(2)]                                  //powerup
    //[Header("POWER UP PURCHASE PANEL Data")]
    //[Space(2)]

    //[SerializeField]
    //private GameObject purchasePowerUPScreen;       //powerup
    //[SerializeField]
    //private Button powerUpBuyBtn;                  //powerup
    //private int currentPowerUpIndex = 0;         //powerup
    //[SerializeField]
    //private GameObject[] powerUpPaels;          //powerup
    //[SerializeField]
    //private TMPro.TextMeshProUGUI txtPrices;


    //public void whenClickPurchasePowerBtn()         //powerup
    //{
    //    purchasePowerUPScreen.SetActive(true);

    //    foreach (var item in powerUpPaels)
    //    {
    //        item.SetActive(false);
    //    }

    //    txtPrices.text = GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost + "";

    //    powerUpPaels[currentPowerUpIndex].SetActive(true);
    //}

    //public void onClosePowerUpPanel()               //powerup  --inspector
    //{
    //    currentPowerUpIndex = 0;
    //    purchasePowerUPScreen.SetActive(false);
    //}

    //public void onCLickPowerUpBuyBtn()          //Not used in the game
    //{
    //    if (Necessary_data.isCoinsAvailable(GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost))
    //    {
    //        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

    //        powerUpBuyBtn.interactable = false;
    //        switch (currentPowerUpIndex)
    //        {

    //            case 0:
    //                StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
    //         0,
    //         0,
    //         powerUpBuyBtn));
    //                break;

    //            case 1:
    //                StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
    //         0,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
    //         0,
    //         powerUpBuyBtn));
    //                break;
    //            case 2:
    //                StartCoroutine(NetworkManager.instance.buyPowerUps(Necessary_data.PlayerID,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Cost,
    //         0,
    //         0,
    //         GameManager.instance.powerUpPurchaseData[currentPowerUpIndex].powerUp_Qty,
    //         powerUpBuyBtn));
    //                break;

    //            default:
    //                break;
    //        }
    //    }
    //    else
    //    {
    //        AllOtherAccess.instance.showPopupWith_Ok_Cancle_Action("You don't have enough coins!\nDo you wants to buy coins?.",
    //            () =>
    //            {
    //                UIManager.instance.showScreenOnly(UIScreens_ENM.Shop);
    //                //  UIManager.instance.hideScreenOnly(UIScreens_ENM.PurchasePowerUpScreen);
    //            }, null);
    //    }
    //}
    //#endregion


    //public IEnumerator submitPlayerAnwser()         //Not used anywhere
    //{
    //    string player_Id = "867780744319725", question_Id = "0F3CD630-70DD-11EA-8267-81A5310DD57B", score = "true", time = "2";

    //    JSONObject json = new JSONObject
    //    {
    //        ["UserID"] = player_Id,
    //        ["QuestionID"] = question_Id,
    //        ["Score"] = true,
    //        ["Time"] = time
    //    };

    //    Debug.Log("----->>> Request For ------> Submit Player Answer : " + NetworkManager.sendPLayerScore + " Parameter : " + json.ToString());
    //    UnityWebRequest quesReq = new UnityWebRequest();
    //    quesReq = UnityWebRequest.Put(NetworkManager.sendPLayerScore, json);
    //    quesReq.method = UnityWebRequest.kHttpVerbPOST;
    //    quesReq.SetRequestHeader("Content-Type", "application/json");
    //    quesReq.SetRequestHeader("Accept", "application/json");

    //    yield return quesReq.SendWebRequest();

    //    if (quesReq.isNetworkError)
    //    {
    //        print("----->>> Response With Error ------> Submit Player Answer");
    //    }
    //    else
    //    {
    //        string response = quesReq.downloadHandler.text;
    //        Debug.Log("----->>> Response Sussess ------> Submit Player Answer ----> " + response);
    //    }
    //}

}
