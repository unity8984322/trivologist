using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.Networking;
using SimpleJSON;

public class LoginScreen : MonoBehaviour
{
    public static LoginScreen instance;

    [SerializeField]
    private TMP_InputField inp_unm, inp_pass;
    [SerializeField]
    private Image passwordShoHide;

    [SerializeField]
    private TextMeshProUGUI txtEmailError, txtPassError;

    public GameObject fastLoginButtons;


    [SerializeField]
    private int passFontSize;
    [SerializeField]
    private int textFontSize;
    public UnityWebRequest result;

    void Start()
    {
        instance = this;
        //nsd = new Necessary_data();
    }
    private void OnEnable()
    {
        txtEmailError.transform.parent.parent.gameObject.SetActive(false);
        txtPassError.transform.parent.parent.gameObject.SetActive(false);
        //txtEmailError.gameObject.SetActive(false);
        //txtPassError.gameObject.SetActive(false);

        inp_unm.text = "";
        inp_pass.text = "";
        inp_pass.contentType = TMP_InputField.ContentType.Password;
        passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;

        if (NetworkManager.instance.wantToEnableFastLogin)
        {
            fastLoginButtons.SetActive(true);
        }
        else
        {
            fastLoginButtons.SetActive(false);
        }
    }

    public void OnInputFienFocue()
    {
        txtEmailError.transform.parent.parent.gameObject.SetActive(false);
        txtPassError.transform.parent.parent.gameObject.SetActive(false);
        //txtEmailError.gameObject.SetActive(false);
        //txtPassError.gameObject.SetActive(false);
    }

    private void showError(TextMeshProUGUI txt, string msg)
    {
        txt.transform.parent.parent.gameObject.SetActive(true);
        txt.text = msg;
    }


    public void hideAllToolTrip()
    {
        txtEmailError.transform.parent.parent.gameObject.SetActive(false);
        txtPassError.transform.parent.parent.gameObject.SetActive(false);
    }

    public void OnClickGuestLogin()
    {
        inp_unm.text = inp_unm.text.Trim();
        inp_pass.text = inp_pass.text.Trim();

        if (!Necessary_data.IsInternetAccessUnavailable())
        {
            if (Necessary_data.isEmptyInputField(inp_unm.text))
            {
                showError(txtEmailError, "Please enter email id.");
            }

            else if (!Necessary_data.isValidEmail(inp_unm.text))
            {
                showError(txtEmailError, Necessary_data.info_InvalidEmail);
            }
            else if (Necessary_data.isEmptyInputField(inp_pass.text))
            {
                showError(txtPassError, "Please enter password.");
            }
            else
            {
                UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
                StartCoroutine(SendLoginQuery());
            }
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action("Unable to login due to network issues", null);
        }
    }

    public void onClickFacebookLogin()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        FacebookManager.instance.FBLogin();
    }

    public void onClickGoogleLogin()
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
        GoogleSignInManager.instance.SignInWithGoogle();
        //GoogleSignInManager.instance.CheckFirebaseDependencies();       
    }


    public void onClickDontHaveAccout()
    {
        hideAllToolTrip();
        UIManager.instance.showNextScreenAndHideCurrentScreen(UIScreens_ENM.signupscreen, UIScreens_ENM.loginscreen);
    }

    public void OnClickBack()
    {
        hideAllToolTrip();
        //        StartSceneUIManager.instance.showNextScreenAndHideCurrentScreen(UIScreens_ENM.startscreen, UIScreens_ENM.loginscreen);
    }

    public void onClickPasswordShowHide()
    {
        string temp_inp_pass = inp_pass.text;
        hideAllToolTrip();
        if (passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon)
        {
            inp_pass.text = "";      //-------
            inp_pass.pointSize = textFontSize;
            inp_pass.contentType = TMP_InputField.ContentType.Standard;
            passwordShoHide.sprite = AllOtherAccess.instance.closedEyeIcon;
            inp_pass.text = temp_inp_pass;      //------
            //inp_pass.Select();        //openskeyboard
            inp_pass.text = temp_inp_pass;
        }
        else
        {
            if (inp_pass.text == "")
            {
                inp_pass.pointSize = textFontSize;
            }
            else
            {
                inp_pass.text = "";
                inp_pass.pointSize = passFontSize;
            }
            inp_pass.contentType = TMP_InputField.ContentType.Password;
            passwordShoHide.sprite = AllOtherAccess.instance.openEyeIcon;
            inp_pass.text = temp_inp_pass;
            inp_pass.text = temp_inp_pass;      //------
            //inp_pass.Select();        //openskeyboard

        }
    }

    public void onEditInpuField(GameObject _obj)
    {
        TMP_InputField tMP_Input = inp_pass;
        Image passwordShoHide = inp_pass.transform.parent.GetChild(0).GetComponent<Image>();


        if (passwordShoHide.sprite == AllOtherAccess.instance.openEyeIcon) // ***** 
        {
            if (tMP_Input.text == "")
            {
                tMP_Input.pointSize = textFontSize;
            }
            else
            {
                tMP_Input.pointSize = passFontSize;
            }
        }
        else //text 
        {
            tMP_Input.pointSize = textFontSize;
        }
    }

    private IEnumerator SendLoginQuery()
    {

        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

        JSONNode jsonData = new JSONObject
        {
            ["email"] = inp_unm.text,
            ["password"] = inp_pass.text//,
            // ["device_id"] = Necessary_data.PlayerDeviceID
        };

        Debug.Log("----->>> Request For ------> LoginWithGmail " + NetworkManager.LoginWithGmail + "  Parameters : " + jsonData.ToString());

        result = UnityWebRequest.Put(NetworkManager.LoginWithGmail, jsonData.ToString());
        result.method = UnityWebRequest.kHttpVerbPOST;
        result.SetRequestHeader("Content-Type", "application/json");
        result.SetRequestHeader("Accept", "application/json");

        yield return result.SendWebRequest();

        if (result.isNetworkError)
        {
            print("----->>> Response With Error ------> Login API " + result.error);
        }
        else
        {
            string response = result.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Login API ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

            if (result.responseCode == 0)
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (010)", null);
            }
            else
            {
                Debug.Log("Result ==> " + result);
                if (results.HasKey("Error"))
                {
                    Debug.Log("results[error] = " + results["Error"]);
                    AllOtherAccess.instance.showPopupWith_Ok_Action(results["Error"], null);
                }
                else if (results.HasKey("status") && results["status"] == 1)
                {
                    if (Necessary_data.isStringNotNullOrEmpty(response))        //------
                    {
                        PlateformLoginManager.instance.SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
                        AllOtherAccess.instance.showPopupWith_Ok_Action(results["message"], null);
                        Debug.Log("Login result = " + results["message"]);
                    }
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Action(results["Message"], null);
                    Debug.Log("Login result = " + results["message"]);
                }

                //    if (result.responseCode == 0)
                //{
                //    AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (006)", null);
                //}
                //else
                //{
                //    if (results["status"] == 1)//login success
                //    {
                //        PlateformLoginManager.instance.SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
                //    }
                //    else
                //    if (results["status"] == 2)//No user exist
                //    {
                //        showError(txtEmailError, Necessary_data.info_EmailNotFound);
                //    }
                //    else
                //    if (results["status"] == 0)//password not matched
                //    {
                //        showError(txtPassError, Necessary_data.info_PasswordNotMatch);
                //    }
                //    else
                //    if (results["status"] == 3)//player already logged-in
                //    {
                //        showError(txtEmailError, results["message"]);
                //    }
                //    else
                //    {
                //        AllOtherAccess.instance.showPopupWith_Ok_Action("Network error please try again ", null);
                //    }
                //}
            }
        }
    }
}
