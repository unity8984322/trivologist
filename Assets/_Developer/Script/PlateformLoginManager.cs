using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlateformLoginManager : MonoBehaviour
{
    public static PlateformLoginManager instance;


    [Tooltip("Set to true on login with either of the Platform")]
    bool isPlatformBasedLogin;
    public bool IsPlatformBasedLogin
    {
        get { return isPlatformBasedLogin; }
        private set { isPlatformBasedLogin = value; }
    }


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }


    public IEnumerator uploadPlayerAvtar(string playerID, string avtarID)
    {
        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

        string query = NetworkManager._updatePlayerAvtar + playerID + "&imageUrl=" + avtarID;

        Debug.Log("avtarID->" + avtarID + " ----->>> Request For ------> UpdatePlayer Avtar " + query.ToString());

        UnityWebRequest result = UnityWebRequest.Put(query, "");
        result.method = UnityWebRequest.kHttpVerbPOST;
        result.SetRequestHeader("Content-Type", "application/json");
        result.SetRequestHeader("Accept", "application/json");

        yield return result.SendWebRequest();

        if (result.isNetworkError)
        {
            print("----->>> Response With Error ------> Login API");
        }
        else
        {
            string response = result.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> UpdatePlayer Avtar ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

            if (result.responseCode == 0)
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (010)", null);
            }
            else
            {
                if (results.HasKey("Error"))
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Action(results["Error"], null);
                }
                else
                {
                    // SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
                }
            }
        }
    }

    /// <summary>
    ///             If GUEST LOGIN Success then set data and goto home screen
    /// </summary>
    /// <param name="response"></param>
    public void SetGuesUserDataOnLoginSuccess(string response)
    {
        Debug.Log("--------->SetGuesUserDataOnLoginSuccess(string response)<---------------" + response);


        JSONNode jsonNode = JSON.Parse(response);

        if (jsonNode.HasKey("status") && jsonNode["status"] == 1)
        {
            //AllOtherAccess.instance.showPopupWith_Ok_Action(jsonNode["message"], null);
            Necessary_data.PlayerID = jsonNode["data"]["id"];
            Necessary_data.PlayerName = jsonNode["data"]["name"];
            Necessary_data.PlayerEmail = jsonNode["data"]["email"];
            Necessary_data.Source = jsonNode["data"]["source"];
            Necessary_data.PlayerCoins = jsonNode["data"]["coins"];
            Necessary_data.PlayerPowerUp_0 = jsonNode["data"]["doubleGuessCount"];
            Necessary_data.PlayerPowerUp_1 = jsonNode["data"]["freeAnswerCount"];
            Necessary_data.PlayerPowerUp_2 = jsonNode["data"]["removeAnswerCount"];


            string profPic = jsonNode["data"]["image"].Value;

            if (Necessary_data.isStringNotNullOrEmpty(profPic))
            {
                Necessary_data.PlayerAvtarID = profPic;
                //   StartCoroutine(uploadPlayerAvtar(Necessary_data.PlayerID, Necessary_data.PlayerAvtarID));
            }
            else
            {
                if (!Necessary_data.isStringNotNullOrEmpty(Necessary_data.PlayerAvtarID))
                {
                    Necessary_data.PlayerAvtarID = "" + UnityEngine.Random.Range(0, 10);
                    StartCoroutine(uploadPlayerAvtar(Necessary_data.PlayerID, Necessary_data.PlayerAvtarID));
                }
            }


            Necessary_data.LoginType = "guest";
            Necessary_data.IsLoggedIn = true;
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
            Debug.Log("Player avtar ID = " + Necessary_data.PlayerAvtarID);
            GameManager.OnProfPicSuccessfullyFetched?.Invoke(getAvtarAsTexture(AllOtherAccess.instance.avtarSprites[int.Parse(Necessary_data.PlayerAvtarID)]));     // param
            //Debug.Log(AllOtherAccess.instance.avtarSprites[int.Parse(Necessary_data.PlayerAvtarID)].name);       //-----

            UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);
            StartCoroutine(NetworkManager.instance.getAllQuiz());

            NetworkManager.instance.playerDetails.player_avtarID = Necessary_data.PlayerAvtarID;
        }
        else
        {
            AllOtherAccess.instance.showPopupWith_Ok_Action(jsonNode["message"]["originalError"]["message"], null);
            UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.loginscreen);
        }
    }

    public Texture2D getAvtarAsTexture(Sprite sp = null)
    {
        //   profilePic.sprite = AllOtherAccess.instance.avtarSprites[int.Parse(Necessary_data.PlayerAvtarID)];     //no need to uncomme
        //sp = AllOtherAccess.instance.avtarSprites[int.Parse(Necessary_data.PlayerAvtarID)];

        var croppedTexture = new Texture2D((int)sp.rect.width, (int)sp.rect.height);
        var pixels = sp.texture.GetPixels((int)sp.textureRect.x,
                                                (int)sp.textureRect.y,
                                                (int)sp.textureRect.width,
                                                (int)sp.textureRect.height);


        //Sprite sprite = AllOtherAccess.instance.avtarSprites[int.Parse(Necessary_data.PlayerAvtarID)];

        //var croppedTexture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
        //var pixels = sprite.texture.GetPixels((int)sprite.textureRect.x,
        //                                        (int)sprite.textureRect.y,
        //                                        (int)sprite.textureRect.width,
        //                                        (int)sprite.textureRect.height);
        croppedTexture.SetPixels(pixels);
        croppedTexture.Apply();
        if (croppedTexture != null && PlayerProfileScreen.clickedOnSetNewAvatar)
            PlayerProfileScreen.instance.setProfilePicture(croppedTexture);     //-------
        return croppedTexture;
    }

    /// <summary>
    /// Get players data received from the response after successfull login
    /// </summary>
    /// <param name="_playerId">The id of the user</param>
    /// <param name="_playerName">The name of the user as per the logged-in account</param>
    /// <param name="_playerEmail">The email of the user as per the logged-in account</param>
    /// <param name="_loginType">The Login Type weather Facebook or Apple</param>

    //GET DATA FROM SOCIAL MEDIA LOGIN RESPONSE
    public void GetUserLoginData(string _playerId, string _playerName, string _playerEmail, string _loginType, string isFirstLogin = "")
    {
        _ = StartCoroutine(LoginWithEitherOfPlatform(_playerId, _playerName, _playerEmail, _loginType));
    }

    //AFTER SOCIALMEDIA LOGIN SUCCESS CALL TO REGISTER PLAYER WITH SOCKET
    [Obsolete]
    public IEnumerator LoginWithEitherOfPlatform(string _playerId, string _playerName, string _playerEmail, string loginType)
    {
        int rdomAvtarID = UnityEngine.Random.Range(0, 8);

        JSONObject json = new JSONObject
        {
            ["id"] = _playerId,
            ["name"] = _playerName,
            ["email"] = _playerEmail
        };


        Debug.Log("----->>> Request For ------> Social Login (" + loginType + ") : " + NetworkManager.socilaLogin + "  Parameter : " + json.ToString());

        UnityWebRequest playerLoginRequest = new UnityWebRequest();
        playerLoginRequest = UnityWebRequest.Put(NetworkManager.socilaLogin, json.ToString());

        playerLoginRequest.method = UnityWebRequest.kHttpVerbPOST;
        playerLoginRequest.SetRequestHeader("Content-Type", "application/json");
        playerLoginRequest.SetRequestHeader("Accept", "application/json");


        yield return playerLoginRequest.SendWebRequest();



        if (playerLoginRequest.isNetworkError)
        {
            print("----->>> Response With Error ------> Social Login");
            AllOtherAccess.instance.showPopupWith_Ok_Action("Check your internet connection", null);     //------
        }
        else
        {
            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);
            Debug.Log("----->>> Response For ------> Social Login : " + playerLoginRequest.downloadHandler.text);

            JSONNode playerLoginJSONResponse = JSON.Parse(playerLoginRequest.downloadHandler.text);
            if (playerLoginJSONResponse.HasKey("Error"))
            {
                Debug.Log("Error while logging in using fb or google = " + playerLoginJSONResponse.Values); ;
                AllOtherAccess.instance.showPopupWith_Ok_Action(playerLoginJSONResponse["Error"], null);
            }
            else
            {

                if (playerLoginJSONResponse.HasKey("status") && playerLoginJSONResponse["status"] == 1)
                {
                    Necessary_data.PlayerID = playerLoginJSONResponse["data"]["id"];
                    Necessary_data.PlayerName = playerLoginJSONResponse["data"]["name"];
                    Necessary_data.PlayerEmail = playerLoginJSONResponse["data"]["email"];
                    Necessary_data.Source = playerLoginJSONResponse["data"]["source"];
                    Necessary_data.PlayerCoins = playerLoginJSONResponse["data"]["coins"];
                    Necessary_data.PlayerPowerUp_0 = playerLoginJSONResponse["data"]["doubleGuessCount"];
                    Necessary_data.PlayerPowerUp_1 = playerLoginJSONResponse["data"]["freeAnswerCount"];
                    Necessary_data.PlayerPowerUp_2 = playerLoginJSONResponse["data"]["removeAnswerCount"];
                    Necessary_data.LoginType = loginType;
                    Necessary_data.IsLoggedIn = true;
                    IsPlatformBasedLogin = true;

                    UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
                    StartCoroutine(downloadAndSetSocilaPic());
                }
                else
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Action(playerLoginRequest.downloadHandler.text, null);
                }
            }
        }
    }

    IEnumerator downloadAndSetSocilaPic()
    {
        WWW url = new WWW(Necessary_data.profilePicURL);// ("https" + "://graph.facebook.com/" + userId + "/picture?type=large"); //+ "?access_token=" + FB.AccessToken);

        Texture2D textFb2 = new Texture2D(128, 128, TextureFormat.DXT1, false); //TextureFormat must be DXT5
        yield return url;

        //  FindObjectOfType<HomeScreen>().profPic_Raw.texture = textFb2;

        GameManager.OnProfPicSuccessfullyFetched?.Invoke(textFb2);

        url.LoadImageIntoTexture(textFb2);

        //Debug.Log("Working");
    }

    //This API is  called only after opening app and last logged in was GUEST type
    public IEnumerator GetUserByEmail(string email)
    {

        UIManager.instance.showScreenOnly(UIScreens_ENM.LOADINGScreen);

        string query = NetworkManager.GetUserByEmail + email;

        Debug.Log("----->>> Request For ------> GetUserByEmail " + query);
        Debug.Log("player Avtar ID-> " + Necessary_data.PlayerAvtarID);

        UnityWebRequest result = UnityWebRequest.Get(NetworkManager.GetUserByEmail + email);

        //UnityWebRequest result = UnityWebRequest.Put(NetworkManager.GetUserByEmail + email, "");
        //result.method = UnityWebRequest.kHttpVerbGET;
        //result.SetRequestHeader("Content-Type", "application/json");
        //result.SetRequestHeader("Accept", "application/json");

        yield return result.SendWebRequest();

        if (result.isNetworkError)
        {
            print("----->>> Response With Error ------> Login API");
            //AllOtherAccess.instance.showPopupWith_Ok_Action("Check your internet connection", null);     //------
        }
        else
        {
            string response = result.downloadHandler.text;
            Debug.Log("----->>> Response Sussess ------> Login API ----> " + response);

            JSONNode results = JSONNode.Parse(response);

            UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

            if (result.responseCode == 0)
            {
                AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (010)", null);
            }
            else
            {
                if (results.HasKey("Error"))
                {
                    AllOtherAccess.instance.showPopupWith_Ok_Action(results["Error"], null);
                }
                else
                {
                    SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
                }



                //    if (result.responseCode == 0)
                //{
                //    AllOtherAccess.instance.showPopupWith_Ok_Action("Server did not respond (006)", null);
                //}
                //else
                //{
                //    if (results["status"] == 1)//login success
                //    {
                //        PlateformLoginManager.instance.SetGuesUserDataOnLoginSuccess(result.downloadHandler.text);
                //    }
                //    else
                //    if (results["status"] == 2)//No user exist
                //    {
                //        showError(txtEmailError, Necessary_data.info_EmailNotFound);
                //    }
                //    else
                //    if (results["status"] == 0)//password not matched
                //    {
                //        showError(txtPassError, Necessary_data.info_PasswordNotMatch);
                //    }
                //    else
                //    if (results["status"] == 3)//player already logged-in
                //    {
                //        showError(txtEmailError, results["message"]);
                //    }
                //    else
                //    {
                //        AllOtherAccess.instance.showPopupWith_Ok_Action("Network error please try again ", null);
                //    }
                //}
            }
        }
    }


}
