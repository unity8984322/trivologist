using System;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using System.Collections.Generic;



[Tooltip("THIS IS OLD SCRIPT NOT USEFULL NOT BUT DO NOT DELETE NOW,")]
public class QuizAdvanture_QuizSelection : MonoBehaviour
{


    /// <summary>
    /// THIS SCRIPT NOT USEFULL NOW BUT DO NOT DELETE NOW
    /// </summary>


    public static QuizAdvanture_QuizSelection instance;



    [SerializeField]
    private TextMeshProUGUI txtCoins;

    [SerializeField]
    private QuizButton QuizBtnPreafab;

    [SerializeField]
    private Transform content;

    [SerializeField]
    private Sprite green, yellow, blue;

    [SerializeField]
    private Color c_green, c_yellow, c_blue;

    public void Awake()
    {
        instance = this;
    }


    public void subscribeEvent()
    {
        GameManager.OnUpdateCoin += setCoins;
    }


    public void unSubscribeEvent()
    {
        GameManager.OnUpdateCoin -= setCoins;
    }

    public void setData()
    {
        AllOtherAccess.instance.ClearChildren(content);

        UIManager.instance.hideScreenOnly(UIScreens_ENM.LOADINGScreen);

        Canvas.ForceUpdateCanvases();

        for (int i = 0; i < GameManager.instance.currentGameQuiz.quizAdvanture.Count; i++)
        {
            spawnButtons(i);
        }
    }

    private void spawnButtons(int i)
    {
        List<int> attendedQusNumers = GameManager.instance.currentGameQuiz.attentdedQuestionNumber;

        QuizButton quizButton = Instantiate(QuizBtnPreafab, content);
        quizButton.txtQuizNum.text = "Quiz " + (1 + i);
        quizButton.GetComponent<Button>().onClick.AddListener(() => quizButton.onClickQuizBtn(i));


        if (attendedQusNumers.Contains(i))
        {
            quizButton.txtQuizNum.color = c_green;
            quizButton.btnBG.sprite = green;
            quizButton.tickMarkImg.SetActive(true);
            quizButton.lockImg.SetActive(false);
        }
        else if (attendedQusNumers.Count > 0 && (i == attendedQusNumers[attendedQusNumers.Count - 1] + 1))
        {
            quizButton.txtQuizNum.color = c_yellow;
            quizButton.btnBG.sprite = yellow;
            quizButton.tickMarkImg.SetActive(false);
            quizButton.lockImg.SetActive(false);
        }
        else
        {
            quizButton.txtQuizNum.color = c_blue;
            quizButton.btnBG.sprite = blue;
            quizButton.tickMarkImg.SetActive(false);
            quizButton.lockImg.SetActive(true);
        }
    }


    private void setCoins()
    {
        txtCoins.text = Necessary_data.PlayerCoins.ToString();
    }

    public void onClickBack()
    {
        UIManager.instance.showNextScreenAndHideAllOtherScreen(UIScreens_ENM.Home);
    }
}
